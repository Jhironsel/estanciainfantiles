package formularios;

import archivos.frmEmpleado;
import archivos.frmLogin;
import archivos.frmImpresoras;
import static clases.Datos.getDatos;
import controlDe.frmAsignacionHorario;
import controlDe.frmHorarios;
import utilidades.DesktopConFondo;
import java.awt.Toolkit;
import javax.swing.SwingWorker;

public final class frmPrincipal extends javax.swing.JFrame {
    /**
     * Metodo construtor de la ventana principal en este se resibe la conexion a
     * la base de datos, tambien tenemos el usuario del sistema y su rol.
     */
    public frmPrincipal() {
        System.out.println("formularios.frmPrincipal.<init>()");
        //Inicializamos las variables del formulario
        initComponents();//Inicializamos todos los componente del formulario.
        //Insertamos Informacion del usuario que tenemos activo en la ventana
        //principal
        txtUsuario.setText("Usuario: " + frmLogin.txtUsuario.getText() + 
                "\n Rol: " + frmLogin.cbRoles.getSelectedItem().toString());

        mbMenu.add(relleno);
        mbMenu.add(jpEstado);
        mbMenu.add(txtUsuario);

        jpEstado.setVisible(false);

        //Centralizamos ventana y ponemos visible        
        setLocationRelativeTo(null);
        setVisible(true);
        
        mnuArchivos.setVisible(true);
        mnuArchivosEmpleados.setVisible(true);
        mnuArchivosImpresoras.setVisible(true);
        mnuArchivoCerrarSeccion.setVisible(true);
        mnuArchivosSalir.setVisible(true);
        
        mnuControlDe.setVisible(true);
        mnuControlHorarios.setVisible(true);
        mnuControlPermisos.setVisible(true);
        
        mnuGestion.setVisible(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtUsuario = new javax.swing.JLabel();
        relleno = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        jpEstado = new javax.swing.JPanel();
        jLabelImpresion = new javax.swing.JLabel();
        jpbEstado = new javax.swing.JProgressBar();
        jScrollPane4 = new javax.swing.JScrollPane();
        dpnEscritorio = new DesktopConFondo();
        mbMenu = new javax.swing.JMenuBar();
        mnuArchivos = new javax.swing.JMenu();
        mnuArchivosEmpleados = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        mnuArchivosImpresoras = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        mnuArchivoCerrarSeccion = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        mnuArchivosSalir = new javax.swing.JMenuItem();
        mnuControlDe = new javax.swing.JMenu();
        mnuControlHorarios = new javax.swing.JMenuItem();
        mnuControlAsignacionHorario = new javax.swing.JMenuItem();
        mnuControlPermisos = new javax.swing.JMenuItem();
        mnuGestion = new javax.swing.JMenu();
        mnuGestionEntrada = new javax.swing.JMenuItem();
        mnuGestionSalida = new javax.swing.JMenuItem();

        txtUsuario.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jpEstado.setMaximumSize(new java.awt.Dimension(400, 30));
        jpEstado.setMinimumSize(new java.awt.Dimension(90, 30));
        jpEstado.setPreferredSize(new java.awt.Dimension(300, 30));
        jpEstado.setLayout(new java.awt.GridLayout(1, 0, 4, 0));

        jLabelImpresion.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabelImpresion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelImpresion.setText("Proceso en curso");
        jLabelImpresion.setMaximumSize(new java.awt.Dimension(120, 25));
        jLabelImpresion.setMinimumSize(new java.awt.Dimension(59, 25));
        jpEstado.add(jLabelImpresion);

        jpbEstado.setFont(new java.awt.Font("Ubuntu", 1, 12)); // NOI18N
        jpbEstado.setDoubleBuffered(true);
        jpbEstado.setMaximumSize(new java.awt.Dimension(32767, 30));
        jpbEstado.setMinimumSize(new java.awt.Dimension(0, 30));
        jpbEstado.setStringPainted(true);
        jpEstado.add(jpbEstado);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Ventana principal del sistema");
        setIconImage(Toolkit.getDefaultToolkit().getImage("icon.png"));
        setMinimumSize(new java.awt.Dimension(640, 480));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        dpnEscritorio.setBackground(new java.awt.Color(0, 102, 102));
        dpnEscritorio.setPreferredSize(new java.awt.Dimension(510, 531));
        jScrollPane4.setViewportView(dpnEscritorio);

        mbMenu.setBackground(new java.awt.Color(195, 226, 252));
        mbMenu.setForeground(new java.awt.Color(1, 1, 1));
        mbMenu.setMinimumSize(new java.awt.Dimension(0, 0));

        mnuArchivos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_Start_Menu_42px.png"))); // NOI18N
        mnuArchivos.setText("Archivos");
        mnuArchivos.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N

        mnuArchivosEmpleados.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_MASK));
        mnuArchivosEmpleados.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        mnuArchivosEmpleados.setForeground(new java.awt.Color(1, 1, 1));
        mnuArchivosEmpleados.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_Admin_42px_1.png"))); // NOI18N
        mnuArchivosEmpleados.setText("Empleados");
        mnuArchivosEmpleados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuArchivosEmpleadosActionPerformed(evt);
            }
        });
        mnuArchivos.add(mnuArchivosEmpleados);
        mnuArchivos.add(jSeparator2);

        mnuArchivosImpresoras.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.ALT_MASK));
        mnuArchivosImpresoras.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        mnuArchivosImpresoras.setForeground(new java.awt.Color(1, 1, 1));
        mnuArchivosImpresoras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_Print_42px.png"))); // NOI18N
        mnuArchivosImpresoras.setText("Impresoras");
        mnuArchivosImpresoras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuArchivosImpresorasActionPerformed(evt);
            }
        });
        mnuArchivos.add(mnuArchivosImpresoras);
        mnuArchivos.add(jSeparator1);

        mnuArchivoCerrarSeccion.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_MASK));
        mnuArchivoCerrarSeccion.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        mnuArchivoCerrarSeccion.setForeground(new java.awt.Color(1, 1, 1));
        mnuArchivoCerrarSeccion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8-salida-42.png"))); // NOI18N
        mnuArchivoCerrarSeccion.setText("Cerrar Seccion");
        mnuArchivoCerrarSeccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuArchivoCerrarSeccionActionPerformed(evt);
            }
        });
        mnuArchivos.add(mnuArchivoCerrarSeccion);
        mnuArchivos.add(jSeparator3);

        mnuArchivosSalir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.ALT_MASK));
        mnuArchivosSalir.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        mnuArchivosSalir.setForeground(new java.awt.Color(1, 1, 1));
        mnuArchivosSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_Shutdown_42px.png"))); // NOI18N
        mnuArchivosSalir.setText("Salir");
        mnuArchivosSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuArchivosSalirActionPerformed(evt);
            }
        });
        mnuArchivos.add(mnuArchivosSalir);

        mbMenu.add(mnuArchivos);

        mnuControlDe.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8-panel-de-control-42.png"))); // NOI18N
        mnuControlDe.setText("Control de...");
        mnuControlDe.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N

        mnuControlHorarios.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        mnuControlHorarios.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        mnuControlHorarios.setForeground(new java.awt.Color(1, 1, 1));
        mnuControlHorarios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8-intervalo-de-tiempo-42.png"))); // NOI18N
        mnuControlHorarios.setText("Horarios");
        mnuControlHorarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuControlHorariosActionPerformed(evt);
            }
        });
        mnuControlDe.add(mnuControlHorarios);

        mnuControlAsignacionHorario.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        mnuControlAsignacionHorario.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        mnuControlAsignacionHorario.setForeground(new java.awt.Color(1, 1, 1));
        mnuControlAsignacionHorario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/AsignacionHorario 42.png"))); // NOI18N
        mnuControlAsignacionHorario.setText("Asignacion Horario");
        mnuControlAsignacionHorario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuControlAsignacionHorarioActionPerformed(evt);
            }
        });
        mnuControlDe.add(mnuControlAsignacionHorario);

        mnuControlPermisos.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        mnuControlPermisos.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        mnuControlPermisos.setForeground(new java.awt.Color(1, 1, 1));
        mnuControlPermisos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8-robo-de-identidad-42.png"))); // NOI18N
        mnuControlPermisos.setText("Permisos");
        mnuControlPermisos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuControlPermisosActionPerformed(evt);
            }
        });
        mnuControlDe.add(mnuControlPermisos);

        mbMenu.add(mnuControlDe);

        mnuGestion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8-boleta-de-calificaciones-32.png"))); // NOI18N
        mnuGestion.setText("Gestión...");
        mnuGestion.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N

        mnuGestionEntrada.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        mnuGestionEntrada.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        mnuGestionEntrada.setForeground(new java.awt.Color(1, 1, 1));
        mnuGestionEntrada.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Entrada.png"))); // NOI18N
        mnuGestionEntrada.setText("Entradas");
        mnuGestionEntrada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuGestionEntradaActionPerformed(evt);
            }
        });
        mnuGestion.add(mnuGestionEntrada);

        mnuGestionSalida.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        mnuGestionSalida.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        mnuGestionSalida.setForeground(new java.awt.Color(1, 1, 1));
        mnuGestionSalida.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Salida.png"))); // NOI18N
        mnuGestionSalida.setText("Salidas");
        mnuGestionSalida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuGestionSalidaActionPerformed(evt);
            }
        });
        mnuGestion.add(mnuGestionSalida);

        mbMenu.add(mnuGestion);

        setJMenuBar(mbMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 553, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        final SwingWorker w = new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                System.out.println("formularios.frmPrincipal.formWindowOpened()");
                ((DesktopConFondo) dpnEscritorio).setImagen("/imagenes/Principal800x600.jpg");
                return null;
            }
        };
        w.execute();
    }//GEN-LAST:event_formWindowOpened

    private void mnuArchivosSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuArchivosSalirActionPerformed
        System.out.println(
                "formularios.frmPrincipal.mnuArchivosSalirActionPerformed()");
        System.exit(0);
    }//GEN-LAST:event_mnuArchivosSalirActionPerformed

    private void mnuArchivosImpresorasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuArchivosImpresorasActionPerformed
        System.out.println(
                "formularios.frmPrincipal.mnuArchivosImpresorasActionPerformed()");
        new frmImpresoras(this, true).setVisible(true);
    }//GEN-LAST:event_mnuArchivosImpresorasActionPerformed

    private void mnuArchivoCerrarSeccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuArchivoCerrarSeccionActionPerformed
        final SwingWorker w = new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                System.out.println(
                        "formularios.frmPrincipal.mnuArchivoCerrarSeccionActionPerformed()");

                frmLogin l = new frmLogin();
                l.setLocationRelativeTo(null);
                l.setVisible(true);
                getDatos("Cerrando la conexion").cerrarConexion();
                getDatos("Nulando la conexion").setConn(null);
                dispose();
                System.gc();
                return null;
            }
        };
        w.execute();
    }//GEN-LAST:event_mnuArchivoCerrarSeccionActionPerformed

    private void mnuArchivosEmpleadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuArchivosEmpleadosActionPerformed
        final SwingWorker w = new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                System.out.println("formularios.frmPrincipal.mnuArchivosEmpleadosActionPerformed()");
                frmEmpleado e = frmEmpleado.getEmpleados();
                dpnEscritorio.remove(e);
                dpnEscritorio.add(e);
                e.setMaximum(true);
                e.setVisible(true);
                return e;
            }
        };
        w.execute();
    }//GEN-LAST:event_mnuArchivosEmpleadosActionPerformed

    private void mnuControlHorariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuControlHorariosActionPerformed
        final SwingWorker w = new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                System.out.println("formularios.frmPrincipal.mnuControlHorariosActionPerformed()");
                frmHorarios h = frmHorarios.getHorarios();
                dpnEscritorio.remove(h);
                dpnEscritorio.add(h);
                h.setMaximum(true);
                h.setVisible(true);
                return h;
            }
        };
        w.execute();
    }//GEN-LAST:event_mnuControlHorariosActionPerformed

    private void mnuGestionEntradaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuGestionEntradaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mnuGestionEntradaActionPerformed

    private void mnuGestionSalidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuGestionSalidaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mnuGestionSalidaActionPerformed

    private void mnuControlPermisosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuControlPermisosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mnuControlPermisosActionPerformed

    private void mnuControlAsignacionHorarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuControlAsignacionHorarioActionPerformed
        final SwingWorker w = new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                System.out.println("formularios.frmPrincipal.mnuControlAsignacionHorarioActionPerformed()");
                frmAsignacionHorario h = frmAsignacionHorario.getAsignacionHorario();
                dpnEscritorio.remove(h);
                dpnEscritorio.add(h);
                h.setMaximum(true);
                h.setVisible(true);
                return h;
            }
        };
        w.execute();
    }//GEN-LAST:event_mnuControlAsignacionHorarioActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JDesktopPane dpnEscritorio;
    public static javax.swing.JLabel jLabelImpresion;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    public static javax.swing.JPanel jpEstado;
    public static javax.swing.JProgressBar jpbEstado;
    private javax.swing.JMenuBar mbMenu;
    private javax.swing.JMenuItem mnuArchivoCerrarSeccion;
    private javax.swing.JMenu mnuArchivos;
    private javax.swing.JMenuItem mnuArchivosEmpleados;
    private javax.swing.JMenuItem mnuArchivosImpresoras;
    private javax.swing.JMenuItem mnuArchivosSalir;
    private javax.swing.JMenuItem mnuControlAsignacionHorario;
    private javax.swing.JMenu mnuControlDe;
    private javax.swing.JMenuItem mnuControlHorarios;
    private javax.swing.JMenuItem mnuControlPermisos;
    private javax.swing.JMenu mnuGestion;
    private javax.swing.JMenuItem mnuGestionEntrada;
    private javax.swing.JMenuItem mnuGestionSalida;
    private javax.swing.Box.Filler relleno;
    public static javax.swing.JLabel txtUsuario;
    // End of variables declaration//GEN-END:variables
}
