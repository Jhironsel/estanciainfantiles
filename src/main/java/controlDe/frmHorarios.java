package controlDe;

import clases.Categoria;
import static clases.Datos.getDatos;
import clases.Horarios;
import clases.TextPrompt;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.logging.Level;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import utilidades.Utilidades;
import static utilidades.Utilidades.LOGGER;
import static utilidades.Utilidades.adjustColumnPreferredWidths;

public class frmHorarios extends javax.swing.JInternalFrame {

    private static frmHorarios horarios;
    private int cliHorarios;
    private int cantHorario;
    private DefaultTableModel dtmEmpleado;
    private TableRowSorter<TableModel> modeloOrdenado;
    private Integer idHorario;

    public frmHorarios() {
        System.out.println("controlDe.frmHorarios.<init>()");
        initComponents();
        mostrarDetalle(true);
        new TextPrompt("Filtro de busqueda", txtFiltroBusqueda);
    }

    public synchronized static frmHorarios getHorarios() {
        System.out.println("controlDe.frmHorarios.getHorarios()");
        if (horarios == null) {
            horarios = new frmHorarios();
        }
        return horarios;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpHorariosGestion = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jsIngresoHoras = new javax.swing.JSpinner();
        jsIngresoMinutos = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jsAlmuerzoHoras = new javax.swing.JSpinner();
        jsAlmuerzoMinutos = new javax.swing.JSpinner();
        jsRegresoHoras = new javax.swing.JSpinner();
        jsRegresoMinutos = new javax.swing.JSpinner();
        jLabel5 = new javax.swing.JLabel();
        jsSalidaHoras = new javax.swing.JSpinner();
        jsSalidaMinutos = new javax.swing.JSpinner();
        jLabel6 = new javax.swing.JLabel();
        jsToleranciaMinutos = new javax.swing.JSpinner();
        jcbEstado = new javax.swing.JCheckBox();
        btnValidaNombre = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jpDetalleHorario = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtHorarios = new JTable(){
            @Override
            public boolean isCellEditable(int rowIndex, int colIndex) { 
                return false; //Las celdas no son editables. 
            }
        };
        jPanel1 = new javax.swing.JPanel();
        jcbEstadosHorarios = new javax.swing.JCheckBox();
        txtFiltroBusqueda = new javax.swing.JTextField();
        jpNavegacion = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        btnPrimero = new javax.swing.JButton();
        btnAnterior = new javax.swing.JButton();
        btnSiguiente = new javax.swing.JButton();
        btnUltimo = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        btnNuevo = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnBorrar = new javax.swing.JButton();

        setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        setClosable(true);
        setIconifiable(true);
        setTitle("Control de horarios");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        jpHorariosGestion.setBorder(javax.swing.BorderFactory.createTitledBorder("Horarios Gestion"));

        jLabel1.setText("Nombre:");

        txtNombre.setToolTipText("Ingrese un nombre unico para el horario");
        txtNombre.setEnabled(false);
        txtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreActionPerformed(evt);
            }
        });
        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreKeyTyped(evt);
            }
        });

        jLabel2.setText("Ingreso:");

        jsIngresoHoras.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jsIngresoHoras.setModel(new javax.swing.SpinnerNumberModel(0, 0, 23, 1));
        jsIngresoHoras.setToolTipText("Marque la hora del ingreso.");
        jsIngresoHoras.setEnabled(false);

        jsIngresoMinutos.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jsIngresoMinutos.setModel(new javax.swing.SpinnerNumberModel(0, 0, 59, 1));
        jsIngresoMinutos.setEnabled(false);

        jLabel3.setText("Almuerzo:");

        jLabel4.setText("Regreso:");

        jsAlmuerzoHoras.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jsAlmuerzoHoras.setModel(new javax.swing.SpinnerNumberModel(0, 0, 23, 1));
        jsAlmuerzoHoras.setEnabled(false);

        jsAlmuerzoMinutos.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jsAlmuerzoMinutos.setModel(new javax.swing.SpinnerNumberModel(0, 0, 59, 1));
        jsAlmuerzoMinutos.setEnabled(false);

        jsRegresoHoras.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jsRegresoHoras.setModel(new javax.swing.SpinnerNumberModel(0, 0, 23, 1));
        jsRegresoHoras.setEnabled(false);

        jsRegresoMinutos.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jsRegresoMinutos.setModel(new javax.swing.SpinnerNumberModel(0, 0, 59, 1));
        jsRegresoMinutos.setEnabled(false);

        jLabel5.setText("Salida:");

        jsSalidaHoras.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jsSalidaHoras.setModel(new javax.swing.SpinnerNumberModel(0, 0, 23, 1));
        jsSalidaHoras.setEnabled(false);

        jsSalidaMinutos.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jsSalidaMinutos.setModel(new javax.swing.SpinnerNumberModel(0, 0, 59, 1));
        jsSalidaMinutos.setEnabled(false);

        jLabel6.setText("Tolerancia: ");

        jsToleranciaMinutos.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jsToleranciaMinutos.setModel(new javax.swing.SpinnerNumberModel(0, 0, 59, 1));
        jsToleranciaMinutos.setEnabled(false);

        jcbEstado.setSelected(true);
        jcbEstado.setText("Activo");
        jcbEstado.setEnabled(false);
        jcbEstado.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jcbEstadoItemStateChanged(evt);
            }
        });

        btnValidaNombre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Aceptar 16 x 16.png"))); // NOI18N
        btnValidaNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnValidaNombreActionPerformed(evt);
            }
        });

        jLabel7.setText("Limite del nombre es de 40 caracteres");

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8-intervalo-de-tiempo-42.png"))); // NOI18N

        javax.swing.GroupLayout jpHorariosGestionLayout = new javax.swing.GroupLayout(jpHorariosGestion);
        jpHorariosGestion.setLayout(jpHorariosGestionLayout);
        jpHorariosGestionLayout.setHorizontalGroup(
            jpHorariosGestionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpHorariosGestionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpHorariosGestionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpHorariosGestionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpHorariosGestionLayout.createSequentialGroup()
                        .addGroup(jpHorariosGestionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jsIngresoHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jsAlmuerzoHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jsRegresoHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jpHorariosGestionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jsRegresoMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jpHorariosGestionLayout.createSequentialGroup()
                                .addComponent(jsAlmuerzoMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jsToleranciaMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jcbEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jpHorariosGestionLayout.createSequentialGroup()
                                .addComponent(jsIngresoMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(37, 37, 37)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jsSalidaHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jsSalidaMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel8))
                    .addGroup(jpHorariosGestionLayout.createSequentialGroup()
                        .addGroup(jpHorariosGestionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNombre)
                            .addGroup(jpHorariosGestionLayout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnValidaNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jpHorariosGestionLayout.setVerticalGroup(
            jpHorariosGestionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpHorariosGestionLayout.createSequentialGroup()
                .addGroup(jpHorariosGestionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpHorariosGestionLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jpHorariosGestionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jLabel1)
                            .addComponent(btnValidaNombre)))
                    .addGroup(jpHorariosGestionLayout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpHorariosGestionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jsIngresoHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jsIngresoMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jsSalidaHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jsSalidaMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpHorariosGestionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jsAlmuerzoMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jsAlmuerzoHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel6)
                    .addComponent(jsToleranciaMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jcbEstado))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpHorariosGestionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpHorariosGestionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                        .addComponent(jLabel4)
                        .addComponent(jsRegresoHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jsRegresoMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(0, 0, 0))
        );

        jpDetalleHorario.setBorder(javax.swing.BorderFactory.createTitledBorder("Detalle de horarios"));

        jtHorarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Nombres", "Ingresos", "Almuerzo", "Regreso", "Salida", "Tolerancia", "Estado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jtHorarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtHorariosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jtHorarios);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtros y estados"));

        jcbEstadosHorarios.setSelected(true);
        jcbEstadosHorarios.setText("Activos");
        jcbEstadosHorarios.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jcbEstadosHorariosItemStateChanged(evt);
            }
        });

        txtFiltroBusqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFiltroBusquedaKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(txtFiltroBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 372, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jcbEstadosHorarios)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcbEstadosHorarios)
                    .addComponent(txtFiltroBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout jpDetalleHorarioLayout = new javax.swing.GroupLayout(jpDetalleHorario);
        jpDetalleHorario.setLayout(jpDetalleHorarioLayout);
        jpDetalleHorarioLayout.setHorizontalGroup(
            jpDetalleHorarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jpDetalleHorarioLayout.setVerticalGroup(
            jpDetalleHorarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpDetalleHorarioLayout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        jpNavegacion.setBorder(javax.swing.BorderFactory.createTitledBorder("Controles"));

        jPanel5.setMinimumSize(new java.awt.Dimension(0, 0));
        jPanel5.setLayout(new java.awt.GridLayout(1, 4, 4, 0));

        btnPrimero.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnPrimero.setForeground(new java.awt.Color(1, 1, 1));
        btnPrimero.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Anterior 32 x 32.png"))); // NOI18N
        btnPrimero.setMnemonic('p');
        btnPrimero.setText("Primero");
        btnPrimero.setToolTipText("Va al Primer Registro");
        btnPrimero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrimeroActionPerformed(evt);
            }
        });
        jPanel5.add(btnPrimero);

        btnAnterior.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnAnterior.setForeground(new java.awt.Color(1, 1, 1));
        btnAnterior.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Flecha Izquierda 32 x 32.png"))); // NOI18N
        btnAnterior.setMnemonic('a');
        btnAnterior.setText("Anterior");
        btnAnterior.setToolTipText("Va al Anterior Registro");
        btnAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnteriorActionPerformed(evt);
            }
        });
        jPanel5.add(btnAnterior);

        btnSiguiente.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnSiguiente.setForeground(new java.awt.Color(1, 1, 1));
        btnSiguiente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Flecha Derecha 32 x 32.png"))); // NOI18N
        btnSiguiente.setMnemonic('s');
        btnSiguiente.setText("Siguiente");
        btnSiguiente.setToolTipText("Va al Siguiente Registro");
        btnSiguiente.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguienteActionPerformed(evt);
            }
        });
        jPanel5.add(btnSiguiente);

        btnUltimo.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnUltimo.setForeground(new java.awt.Color(1, 1, 1));
        btnUltimo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Siguiente 32 x 32.png"))); // NOI18N
        btnUltimo.setMnemonic('u');
        btnUltimo.setText("Ultimo");
        btnUltimo.setToolTipText("Va al Ultimo Registro");
        btnUltimo.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnUltimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUltimoActionPerformed(evt);
            }
        });
        jPanel5.add(btnUltimo);

        jPanel6.setMinimumSize(new java.awt.Dimension(0, 0));
        jPanel6.setLayout(new java.awt.GridLayout(1, 0, 4, 0));

        btnNuevo.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnNuevo.setForeground(new java.awt.Color(1, 1, 1));
        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Documento nuevo 32 x 32.png"))); // NOI18N
        btnNuevo.setMnemonic('n');
        btnNuevo.setText("Nuevo");
        btnNuevo.setToolTipText("Crear un nuevo Registro");
        btnNuevo.setMaximumSize(new java.awt.Dimension(104, 44));
        btnNuevo.setMinimumSize(new java.awt.Dimension(104, 44));
        btnNuevo.setPreferredSize(new java.awt.Dimension(104, 44));
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        jPanel6.add(btnNuevo);

        btnModificar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnModificar.setForeground(new java.awt.Color(1, 1, 1));
        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Editar Documento 32 x 32.png"))); // NOI18N
        btnModificar.setMnemonic('m');
        btnModificar.setText("Modificar");
        btnModificar.setToolTipText("Modificar Registro Actual");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });
        jPanel6.add(btnModificar);

        btnGuardar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnGuardar.setForeground(new java.awt.Color(1, 1, 1));
        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Guardar 32 x 32.png"))); // NOI18N
        btnGuardar.setMnemonic('g');
        btnGuardar.setText("Guardar");
        btnGuardar.setToolTipText("Guardar Registro Actual");
        btnGuardar.setEnabled(false);
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        jPanel6.add(btnGuardar);

        btnCancelar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(1, 1, 1));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Cancelar 32 x 32.png"))); // NOI18N
        btnCancelar.setMnemonic('c');
        btnCancelar.setText("Cancelar");
        btnCancelar.setToolTipText("Cancela la Operacion del Registro");
        btnCancelar.setEnabled(false);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        jPanel6.add(btnCancelar);

        btnBorrar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnBorrar.setForeground(new java.awt.Color(1, 1, 1));
        btnBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Borrar 32 x 32.png"))); // NOI18N
        btnBorrar.setMnemonic('b');
        btnBorrar.setText("Borrar");
        btnBorrar.setToolTipText("Borrar Registro Actual");
        btnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarActionPerformed(evt);
            }
        });
        jPanel6.add(btnBorrar);

        javax.swing.GroupLayout jpNavegacionLayout = new javax.swing.GroupLayout(jpNavegacion);
        jpNavegacion.setLayout(jpNavegacionLayout);
        jpNavegacionLayout.setHorizontalGroup(
            jpNavegacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jpNavegacionLayout.setVerticalGroup(
            jpNavegacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpNavegacionLayout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpHorariosGestion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jpDetalleHorario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jpNavegacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jpHorariosGestion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jpDetalleHorario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jpNavegacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnPrimeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrimeroActionPerformed
        System.out.println("controlDe.frmHorarios.btnPrimeroActionPerformed()");
        if (!jtHorarios.isEnabled()) {
            return;
        }
        cliHorarios = 0;

        btnPrimero.requestFocusInWindow();
    }//GEN-LAST:event_btnPrimeroActionPerformed

    private void btnAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnteriorActionPerformed
        System.out.println("controlDe.frmHorarios.btnAnteriorActionPerformed()");
        if (!jtHorarios.isEnabled()) {
            return;
        }
        cliHorarios--;
        if (cliHorarios == -1) {
            cliHorarios = cantHorario - 1;
        }
        btnAnterior.requestFocusInWindow();
    }//GEN-LAST:event_btnAnteriorActionPerformed

    private void btnSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguienteActionPerformed
        System.out.println("controlDe.frmHorarios.btnSiguienteActionPerformed()");
        if (!jtHorarios.isEnabled()) {
            return;
        }
        cliHorarios++;
        if (cliHorarios == cantHorario) {
            cliHorarios = 0;
        }
        btnSiguiente.requestFocusInWindow();
    }//GEN-LAST:event_btnSiguienteActionPerformed

    private void btnUltimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUltimoActionPerformed
        System.out.println("controlDe.frmHorarios.btnUltimoActionPerformed()");
        if (!jtHorarios.isEnabled()) {
            return;
        }
        cliHorarios = cantHorario - 1;

        btnUltimo.requestFocusInWindow();
    }//GEN-LAST:event_btnUltimoActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        System.out.println("controlDe.frmHorarios.btnNuevoActionPerformed()");
        //Activamos el Flag de registro Nuevo
        idHorario = null;
        mostrarDetalle(false);

        datosDetalles(true);

        navegador(false);

        jsEnabled(false);

        btnCancelar.setEnabled(true);
        btnGuardar.setEnabled(false);

        btnValidaNombre.setEnabled(true);

        txtNombre.setEnabled(true);
        txtNombre.requestFocusInWindow();
        txtNombre.setText("");
        Utilidades.showTooltip(txtNombre);
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        System.out.println("controlDe.frmHorarios.btnModificarActionPerformed()");
        if (jtHorarios.getRowCount() == 0) {
            JOptionPane.showInternalMessageDialog(this,
                    "No se encuentra datos en la tabla. Registre un horario",
                    "Proceso de validacion de datos",
                    JOptionPane.DEFAULT_OPTION);
            return;
        }
        if (jtHorarios.getSelectedRow() == -1) {
            JOptionPane.showInternalMessageDialog(this,
                    "Debe seleccionar un registro de la tabla",
                    "Proceso de validacion de datos",
                    JOptionPane.DEFAULT_OPTION);
            jtHorarios.requestFocusInWindow();
            return;
        }
        jsEnabled(true);
        mostrarRegistro(true);
        mostrarDetalle(false);
        navegador(false);
        btnValidaNombre.setEnabled(false);
        idHorario = ((Categoria) jtHorarios.getValueAt(cliHorarios, 0)).getIdCategoria();
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        System.out.println("controlDe.frmHorarios.btnGuardarActionPerformed()");
        //Validaciones de datos...

        if (txtNombre.getText().equals("")) {
            JOptionPane.showInternalConfirmDialog(this,
                    "Inserte el Nombre!!!", "Olvida algo!",
                    JOptionPane.DEFAULT_OPTION);
            txtNombre.requestFocusInWindow();
            return;
        }

        if (!jcbEstado.isSelected()) {
            int resp = JOptionPane.showInternalConfirmDialog(
                    this,
                    "Desea dejar horario inactivo?",
                    "Proceso de validacion",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (resp == JOptionPane.NO_OPTION) {
                jcbEstado.setSelected(true);
                return;
            }
        }
        //FIN de las Validaciones..........................
        Time ing = new Time(
                (int) jsIngresoHoras.getValue(),
                (int) jsIngresoMinutos.getValue(), 00);
        Time alm = new Time(
                (int) jsAlmuerzoHoras.getValue(),
                (int) jsAlmuerzoMinutos.getValue(), 00);
        Time reg = new Time(
                (int) jsRegresoHoras.getValue(),
                (int) jsRegresoMinutos.getValue(), 00);
        Time sal = new Time(
                (int) jsSalidaHoras.getValue(),
                (int) jsSalidaMinutos.getValue(), 00);

        Horarios h = new Horarios(
                idHorario,
                txtNombre.getText(),
                ing,
                alm,
                reg,
                sal,
                (int) jsToleranciaMinutos.getValue(),
                jcbEstado.isSelected());

        JOptionPane.showInternalConfirmDialog(
                this,
                getDatos("Agregar horario al sistema").
                        agregarHorario(h),
                "Agregando horario de empleados",
                JOptionPane.DEFAULT_OPTION);

        llenarTabla(jcbEstadosHorarios.isSelected());
        btnCancelarActionPerformed(null);
        cantHorario = getDatos("Obteniendo cantidad de empleados del sistema 2").
                cantHorarios(jcbEstadosHorarios.isSelected());
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        System.out.println("controlDe.frmHorarios.btnCancelarActionPerformed()");
        //Desactivamos el Flag de registro Nuevo
        jcbEstadosHorariosItemStateChanged(null);
        datosDetalles(false);
        jsEnabled(false);
        mostrarDetalle(true);
        navegador(true);
        btnPrimeroActionPerformed(null);
        btnCancelar.requestFocusInWindow();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarActionPerformed
        System.out.println("controlDe.frmHorarios.btnBorrarActionPerformed()");
        if (jtHorarios.getSelectedRow() == -1) {
            JOptionPane.showInternalMessageDialog(this,
                    "Debe de seleccionar un registro de la tabla!!!",
                    "Proceso de validación",
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        int resp = JOptionPane.showInternalConfirmDialog(this,
                "Desea borrar el Horario "
                + (((Categoria) jtHorarios.getValueAt(cliHorarios, 0)).
                        getDescripcion().startsWith("Horario ")
                ? ((Categoria) jtHorarios.getValueAt(cliHorarios, 0)).
                        getDescripcion().replaceAll("Horario ", "")
                : ((Categoria) jtHorarios.getValueAt(cliHorarios, 0)).
                        getDescripcion())
                + "\nDesea Continuar?",
                "Proceso de validacion de borrado",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.INFORMATION_MESSAGE);
        if (resp == JOptionPane.NO_OPTION) {
            return;
        }

        JOptionPane.showMessageDialog(this,
                getDatos("Borrando empleado del sistema").borrarHorario(
                        ((Categoria) jtHorarios.getValueAt(cliHorarios, 0)).
                                getIdCategoria()),
                "Resultado de proceso!",
                JOptionPane.INFORMATION_MESSAGE);

        cliHorarios = 0;
        //Actualizamos los cambios en la Tabla
        llenarTabla(jcbEstado.isSelected());
        cantHorario = getDatos("Obteniendo la cantidad de empleados del sistema 3").
                cantHorarios(jcbEstadosHorarios.isSelected());
        btnBorrar.requestFocusInWindow();
    }//GEN-LAST:event_btnBorrarActionPerformed

    private void btnValidaNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnValidaNombreActionPerformed
        System.out.println("controlDe.frmHorarios.btnValidaNombreActionPerformed()");
        if (txtNombre.getText().isEmpty()) {
            JOptionPane.showInternalMessageDialog(this,
                    "Debe ingresar el nombre del horario, a registrar.");
            return;
        }

        if (getDatos("Consultar si existe horario").existeHorario(
                txtNombre.getText().trim())) {
            JOptionPane.showInternalMessageDialog(this,
                    "Este perfil de horario ya existe");
            txtNombre.setText("");
            return;
        }
        jsEnabled(true);
        mostrarDetalle(false);
        jsIngresoHoras.requestFocusInWindow();
        jsIngresoHoras.requestFocus();
        Utilidades.showTooltip(jsIngresoHoras);

        btnGuardar.setEnabled(true);
        System.out.println("Fin");
    }//GEN-LAST:event_btnValidaNombreActionPerformed

    private void txtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreActionPerformed
        System.out.println("controlDe.frmHorarios.txtNombreActionPerformed()");
        btnValidaNombreActionPerformed(evt);
    }//GEN-LAST:event_txtNombreActionPerformed

    private void txtNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyTyped
        System.out.println("controlDe.frmHorarios.txtNombreKeyTyped()");
        if (txtNombre.getText().length() >= 40) {
            evt.consume();
        }
    }//GEN-LAST:event_txtNombreKeyTyped

    private void jcbEstadosHorariosItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jcbEstadosHorariosItemStateChanged
        System.out.println("controlDe.frmHorarios.jcbEstadosHorariosItemStateChanged()");
        if (jcbEstadosHorarios.isSelected()) {
            jcbEstadosHorarios.setText("Activo");
        } else {
            jcbEstadosHorarios.setText("Inactivo");
        }
        llenarTabla(jcbEstadosHorarios.isSelected());
    }//GEN-LAST:event_jcbEstadosHorariosItemStateChanged

    private void jcbEstadoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jcbEstadoItemStateChanged
        System.out.println("controlDe.frmHorarios.jcbEstadoItemStateChanged()");
        if (jcbEstado.isSelected()) {
            jcbEstado.setText("Activo");
        } else {
            jcbEstado.setText("Inactivo");
        }
    }//GEN-LAST:event_jcbEstadoItemStateChanged

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        System.out.println("controlDe.frmHorarios.formInternalFrameOpened()");
        llenarTabla(jcbEstadosHorarios.isSelected());
    }//GEN-LAST:event_formInternalFrameOpened

    private void jtHorariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtHorariosMouseClicked
        System.out.println("controlDe.frmHorarios.jtHorariosMouseClicked()");
        if (!jtHorarios.isEnabled()) {
            return;
        }
        cliHorarios = jtHorarios.getSelectedRow();
        if (evt.getClickCount() == 2) {
            btnModificarActionPerformed(null);
        }
    }//GEN-LAST:event_jtHorariosMouseClicked

    private void txtFiltroBusquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFiltroBusquedaKeyReleased
        System.out.println("controlDe.frmHorarios.txtFiltroBusquedaKeyReleased()");
        modeloOrdenado.setRowFilter(RowFilter.regexFilter("(?i)" + txtFiltroBusqueda.getText(), 0));
    }//GEN-LAST:event_txtFiltroBusquedaKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnterior;
    private static javax.swing.JButton btnBorrar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnPrimero;
    private javax.swing.JButton btnSiguiente;
    private javax.swing.JButton btnUltimo;
    private javax.swing.JButton btnValidaNombre;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JCheckBox jcbEstado;
    private javax.swing.JCheckBox jcbEstadosHorarios;
    private javax.swing.JPanel jpDetalleHorario;
    private javax.swing.JPanel jpHorariosGestion;
    private javax.swing.JPanel jpNavegacion;
    private javax.swing.JSpinner jsAlmuerzoHoras;
    private javax.swing.JSpinner jsAlmuerzoMinutos;
    private javax.swing.JSpinner jsIngresoHoras;
    private javax.swing.JSpinner jsIngresoMinutos;
    private javax.swing.JSpinner jsRegresoHoras;
    private javax.swing.JSpinner jsRegresoMinutos;
    private javax.swing.JSpinner jsSalidaHoras;
    private javax.swing.JSpinner jsSalidaMinutos;
    private javax.swing.JSpinner jsToleranciaMinutos;
    private javax.swing.JTable jtHorarios;
    private javax.swing.JTextField txtFiltroBusqueda;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables

    private void mostrarRegistro(boolean b) {
        System.out.println("controlDe.frmHorarios.mostrarRegistro()");
        if (jtHorarios.getRowCount() == 0) {
            datosDetalles(true);
            return;
        }
        jtHorarios.setRowSelectionInterval(cliHorarios, cliHorarios);
        txtNombre.setText(((Categoria) jtHorarios.getValueAt(cliHorarios, 0)).getDescripcion());
        jsIngresoHoras.setValue(((Time) jtHorarios.getValueAt(cliHorarios, 1)).getHours());
        jsIngresoMinutos.setValue(((Time) jtHorarios.getValueAt(cliHorarios, 1)).getMinutes());

        jsAlmuerzoHoras.setValue(((Time) jtHorarios.getValueAt(cliHorarios, 2)).getHours());
        jsAlmuerzoMinutos.setValue(((Time) jtHorarios.getValueAt(cliHorarios, 2)).getMinutes());

        jsRegresoHoras.setValue(((Time) jtHorarios.getValueAt(cliHorarios, 3)).getHours());
        jsRegresoMinutos.setValue(((Time) jtHorarios.getValueAt(cliHorarios, 3)).getMinutes());

        jsSalidaHoras.setValue(((Time) jtHorarios.getValueAt(cliHorarios, 4)).getHours());
        jsSalidaMinutos.setValue(((Time) jtHorarios.getValueAt(cliHorarios, 4)).getMinutes());

        jsToleranciaMinutos.setValue(jtHorarios.getValueAt(cliHorarios, 5));

        jcbEstado.setSelected(jcbEstadosHorarios.isSelected());
    }

    private void llenarTabla(boolean b) {
        System.out.println("controlDe.frmHorarios.llenarTabla()");
        cliHorarios = 0;
        jtHorarios.removeAll();
        String titulos[] = {
            "<html><b>Nombres</b></html>",
            "<html><b>Ingresos</b></html>",
            "<html><b>Almuerzo</b></html>",
            "<html><b>Regreso</b></html>",
            "<html><b>Salida</b></html>",
            "<html><b>Tolerancia</b></html>"};
        Object registro[] = new Object[titulos.length];
        try {
            ResultSet rs = getDatos("Llenar la tabla de los horarios").
                    getHorariosEstado(jcbEstadosHorarios.isSelected());
            dtmEmpleado = new DefaultTableModel(null, titulos);
            while (rs.next()) {
                registro[0] = new Categoria(rs.getInt("idhorario"),
                        rs.getString("descripcion").trim());
                registro[1] = rs.getTime("Ingreso");
                registro[2] = rs.getTime("Almuerzo");
                registro[3] = rs.getTime("Reingreso");
                registro[4] = rs.getTime("Salida");
                registro[5] = rs.getInt("Tolerancia");
                dtmEmpleado.addRow(registro);
            }
            jtHorarios.setModel(dtmEmpleado);
            modeloOrdenado = new TableRowSorter<TableModel>(dtmEmpleado);
            jtHorarios.setRowSorter(modeloOrdenado);
        } catch (SQLException ex) {
            LOGGER.getLogger(frmHorarios.class.getName()).log(Level.SEVERE, null, ex);
        }
        adjustColumnPreferredWidths(jtHorarios);
    }

    private void navegador(boolean b) {
        System.out.println("controlDe.frmHorarios.navegador()");
        txtNombre.requestFocusInWindow();
        btnPrimero.setEnabled(b);
        btnAnterior.setEnabled(b);
        btnSiguiente.setEnabled(b);
        btnUltimo.setEnabled(b);
        btnNuevo.setEnabled(b);
        btnModificar.setEnabled(b);
        btnBorrar.setEnabled(b);

        //Caja de Texto Habilitado
        btnGuardar.setEnabled(!b);
        btnCancelar.setEnabled(!b);
        btnValidaNombre.setEnabled(!b);
    }

    private void datosDetalles(boolean b) {
        System.out.println("controlDe.frmHorarios.datosDetalles()");
        if (b) {
            txtNombre.setText("");
            jsIngresoHoras.setValue(0);
            jsIngresoMinutos.setValue(0);
            jsAlmuerzoHoras.setValue(0);
            jsAlmuerzoMinutos.setValue(0);
            jsRegresoHoras.setValue(0);
            jsRegresoMinutos.setValue(0);
            jsSalidaHoras.setValue(0);
            jsSalidaMinutos.setValue(0);
            jsToleranciaMinutos.setValue(0);
            jcbEstado.setSelected(true);
            return;
        }
    }

    private void jsEnabled(boolean b) {
        System.out.println("controlDe.frmHorarios.jsEnabled()");
        txtNombre.setEnabled(b);
        jsIngresoHoras.setEnabled(b);
        jsIngresoMinutos.setEnabled(b);
        jsAlmuerzoHoras.setEnabled(b);
        jsAlmuerzoMinutos.setEnabled(b);
        jsRegresoHoras.setEnabled(b);
        jsRegresoMinutos.setEnabled(b);
        jsSalidaHoras.setEnabled(b);
        jsSalidaMinutos.setEnabled(b);
        jsToleranciaMinutos.setEnabled(b);
        jcbEstado.setEnabled(b);
    }

    private void mostrarDetalle(boolean b) {
        System.out.println("controlDe.frmHorarios.mostrarDetalle()");
        jpDetalleHorario.setVisible(b);
        jpHorariosGestion.setVisible(!b);
    }
}
