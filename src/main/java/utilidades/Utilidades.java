package utilidades;

import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;

public class Utilidades {

    public static final Logger LOGGER = Logger.getLogger(Utilidades.class.getName());

    static {
        FileHandler fh = null;
        try {
            fh = new FileHandler(System.getProperty("user.dir") + "/TestLog.log", true);
            //fh.setFormatter(new XMLFormatter());
            fh.setFormatter(new SimpleFormatter());
            LOGGER.addHandler(fh);

        } catch (IOException e) {
            System.out.print(e.getLocalizedMessage());
        } finally {
            fh.close();
        }
    }

    //Metodo utilizado para definir las teclas de
    //acceso rapido en el formulario principal....
    public static void clickOnKey(final AbstractButton button, String actionName, int key) {
        button.getInputMap(JButton.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(key, 0), actionName);
        button.getActionMap().put(actionName, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button.doClick();
            }
        });
    }

    public static void extractPrintImage(String filePath, JasperPrint print) throws JRException {
        try {
            OutputStream ouputStream = new FileOutputStream(filePath);
            DefaultJasperReportsContext.getInstance();
            JasperPrintManager printManager = JasperPrintManager.getInstance(DefaultJasperReportsContext.getInstance());

            BufferedImage rendered_image = (BufferedImage) printManager.printToImage(print, 0, 1.6f);
            ImageIO.write(rendered_image, "png", ouputStream);

        } catch (IOException | JRException e) {
            JOptionPane.showMessageDialog(null, e.getLocalizedMessage());
        }
    }

    public static double controlDouble(Object longValue) {
        double valueTwo = -99; // whatever to state invalid!
        if (longValue instanceof Number) {
            valueTwo = ((Number) longValue).doubleValue();
        }
        return valueTwo;
    }

    public static void copyFileUsingFileChannels(String source, String dest) {
        try {
            InputStream in = new FileInputStream(source);
            OutputStream out = new FileOutputStream(dest);

            byte[] buf = new byte[1024];
            int len;

            while ((len = in.read(buf)) > 0) {
                //out.flush();
                out.write(buf, 0, len);
            }

            in.close();
            out.close();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, e.getLocalizedMessage());
        }
    }

    public static boolean isNumercFloat(String cadena) {
        String cadena1 = cadena.replace("$", "").replace(" ", "");
        try {
            Float.parseFloat(cadena1);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public static java.sql.Date javaDateToSqlDate(java.util.Date date) {
        return new java.sql.Date(date.getTime());
    }

    public static String formatDate(Date fecha, String tipo) {
        if (fecha == null) {
            fecha = new Date();
        }
        SimpleDateFormat formatoDelTexto;
        switch (tipo) {
            case "dd-MM-yyyy":
                formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
                break;
            case "MM-dd-yyyy":
                formatoDelTexto = new SimpleDateFormat("MM-dd-yyyy");
                break;
            case "dd/MM/yyyy":
                formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
                break;
            default:
                formatoDelTexto = new SimpleDateFormat("dd.MM.yyyy");
                break;
        }

        return formatoDelTexto.format(fecha);
    }

    public static Date objectToDate(Object obj) {
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("MM/dd/yyyy");
        Date aux = null;
        try {
            aux = formatoDelTexto.parse(objectToString(obj));
        } catch (ParseException ex) {
            System.out.print(ex.getLocalizedMessage());
        }
        return aux;
    }

    public static double objectToDouble(Object Obj) {
        String Str = Obj.toString();
        String aux = Str.replace("R", "").replace("D", "").replace("$", "").trim();
        return Double.parseDouble(aux);
    }

    public static int objectToInt(Object Obj) {
        String Str = Obj.toString();
        return Integer.parseInt(Str);
    }

    public static String objectToString(Object Obj) {
        String Str = "";
        if (Obj != null) {
            Str = Obj.toString();
        }
        return Str;
    }

    public static String priceWithDecimal(double price) {
        DecimalFormat formatter = new DecimalFormat("#0.00");
        return formatter.format(price);
    }

    public static void limpiarPantalla() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static void showTooltip(Component component) {
        final ToolTipManager ttm = ToolTipManager.sharedInstance();
        final int oldDelay = ttm.getInitialDelay();
        ttm.setInitialDelay(0);
        ttm.mouseMoved(new MouseEvent(component, 0, 0, 0,
                0, 0, // X-Y of the mouse for the tool tip
                0, false));
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ttm.setInitialDelay(oldDelay);
            }
        });
    }

    public static void adjustColumnPreferredWidths(JTable table) {
        // strategy - get max width for cells in column and
        // make that the preferred width
        TableColumnModel columnModel = table.getColumnModel();
        for (int col = 0; col < table.getColumnCount(); col++) {
            int maxwidth = 0;
            for (int row = 0; row < table.getRowCount(); row++) {
                TableCellRenderer rend = table.getCellRenderer(row, col);
                Object value = table.getValueAt(row, col);
                Component comp = rend.getTableCellRendererComponent(table,
                        value,
                        false,
                        false,
                        row,
                        col);
                maxwidth = Math.max(comp.getPreferredSize().width, maxwidth);
            } // for row                 
            TableColumn column = columnModel.getColumn(col);
            if (col == 6) {
                maxwidth += 50;
            }
            column.setPreferredWidth(maxwidth + 20);
        }
    }

    public static boolean validarCedula(String cedula) {
        int suma = 0;
        final char[] peso = {'1', '2', '1', '2', '1', '2', '1', '2', '1', '2'};
        cedula = cedula.replace("-", "");
        // Comprobaciones iniciales
        if ((cedula == null) || (cedula.length() != 11)) {
            return false;
        }
        // Si no es un nº => la descartamos  
        try {
            Long.parseLong(cedula);
        } catch (Exception e) {
            return false;
        }
        
        for (int i = 0; i < 10; i++) {
            int a = Character.getNumericValue(cedula.charAt(i));
            int b = Character.getNumericValue(peso[i]);
            char[] mult = Integer.toString(a * b).toCharArray();

            if (mult.length > 1) {
                a = Character.getNumericValue(mult[0]);
                b = Character.getNumericValue(mult[1]);
            } else {
                a = 0;
                b = Character.getNumericValue(mult[0]);
            }
            suma = suma + a + b;
        }
        int digito = (10 - (suma % 10)) % 10;
        // Comprobamos que el dígito de control coincide    
        if (digito != Character.getNumericValue(cedula.charAt(10))) {
            return false;
        }
        return true;
    }
    
    public static boolean validaCorreo(String correo){
        // Patrón para validar el email
        Pattern pattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher mather = pattern.matcher(correo); 
        if (mather.find() == true) {
            System.out.println("El email ingresado es válido.");
            return true;
        } 
        return false;
    }
    
    public static Image iconToImage(Icon icon) {
          if (icon instanceof ImageIcon) {
              return ((ImageIcon)icon).getImage();
          } else {
              int w = icon.getIconWidth();
              int h = icon.getIconHeight();
              GraphicsEnvironment ge =
                GraphicsEnvironment.getLocalGraphicsEnvironment();
              GraphicsDevice gd = ge.getDefaultScreenDevice();
              GraphicsConfiguration gc = gd.getDefaultConfiguration();
              BufferedImage image = gc.createCompatibleImage(w, h);
              Graphics2D g = image.createGraphics();
              icon.paintIcon(null, g, 0, 0);
              g.dispose();
              return image;
          }
      }
}
