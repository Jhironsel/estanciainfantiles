package clases;

import java.sql.Time;

public class Horarios {
    private Integer idHorario;
    private String descripcion;
    private Time ingreso;
    private Time almuerzo;
    private Time reingreso;
    private Time salida;
    private int tolerancia;
    private boolean estado;

    public Horarios(Integer idHorario, String descripcion, Time ingreso,
            Time almuerzo, Time reingreso, Time salida, 
            int tolerancia, boolean estado) {
        this.idHorario = idHorario;
        this.descripcion = descripcion;
        this.ingreso = ingreso;
        this.almuerzo = almuerzo;
        this.reingreso = reingreso;
        this.salida = salida;
        this.tolerancia = tolerancia;
        this.estado = estado;
    }
    
    public Integer getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(Integer idHorario) {
        this.idHorario = idHorario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Time getIngreso() {
        return ingreso;
    }

    public void setIngreso(Time ingreso) {
        this.ingreso = ingreso;
    }

    public Time getAlmuerzo() {
        return almuerzo;
    }

    public void setAlmuerzo(Time almuerzo) {
        this.almuerzo = almuerzo;
    }

    public Time getReingreso() {
        return reingreso;
    }

    public void setReingreso(Time reingreso) {
        this.reingreso = reingreso;
    }

    public int getTolerancia() {
        return tolerancia;
    }

    public void setTolerancia(int tolerancia) {
        this.tolerancia = tolerancia;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Time getSalida() {
        return salida;
    }

    public void setSalida(Time salida) {
        this.salida = salida;
    }
    
    
}
