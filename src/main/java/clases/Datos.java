package clases;

import Clases.Colores;
import archivos.Empleado;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import org.apache.commons.codec.binary.Base64;
import static utilidades.Utilidades.LOGGER;

public class Datos {

    private Connection conn;
    private static Datos miDato;//Una variable propia del formulario
    private String sql;

    public synchronized static Datos getDatos(String operacion) {
        System.out.println(Colores.ANSI_RED + "Conexion :" + operacion + Colores.ANSI_RESET);
        if (miDato == null) {
            miDato = new Datos();
        }
        return miDato;
    }

    public String getSql() {
        System.out.println("SQL: " + Colores.ANSI_PURPLE_BACKGROUND + Colores.ANSI_YELLOW + sql + Colores.ANSI_RESET);
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public synchronized Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        System.out.println(Colores.ANSI_GREEN + Colores.ANSI_BLACK_BACKGROUND
                + "clases.Datos.setConn()" + Colores.ANSI_RESET);
        this.conn = conn;
    }

    /**
     * Metodo para validar e iniciar el sistema y la base de datos.
     *
     * @param user nombre de usuario para realizar la conexion
     * @param pass valor de la contrasena del sistema
     * @param rol que permite identificar las operaciones que puede realizar el
     * usuario.
     * @return Devuelve un valor booleano que indica si la conexion fue exitosa
     * o no.
     */
    public synchronized boolean valida(String user, String pass, String rol) {
        System.out.println("clases.Datos.valida()");
        Properties props = new Properties();
        props.setProperty("user", user.toUpperCase());
        props.setProperty("password", pass);
        props.setProperty("charSet", "UTF8");

        if (!"None".equals(rol)) {
            System.out.println("El ROLE: " + rol);
            props.setProperty("roleName", rol);
            props.setProperty("role", rol);
            props.setProperty("rol", rol);
        }

        try {
            Class.forName("org.firebirdsql.jdbc.FBDriver");
        } catch (ClassNotFoundException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "No se encuentra el driver JBird!");
            return false;
        }

        FileReader fr = null;

        try {
            fr = new FileReader("src/main/resources/direccionLinux.txt");
        } catch (FileNotFoundException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "No se encuentra los archivos de dominios");
            return false;
        }

        BufferedReader br = new BufferedReader(fr);
        String linea = "";

        try {
            while ((linea = br.readLine()) != null) {
                try {
                    setConn(DriverManager.getConnection(
                            "jdbc:firebirdsql://" + linea + "/estancia", props));
                    return true;
                } catch (SQLException ex) {
                    System.out.println("Se probo con: " + linea);
                }
            }
            return false;
        } catch (IOException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fr.close();
            } catch (IOException ex) {
                LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    public synchronized void cerrarConexion() {
        System.out.println("clases.Datos.cerrarConexion()");
        if (getConn() == null) {
            return;
        }
        try {
            getConn().close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Metodos que devuelven entero.
    public synchronized int cantEmpleados(boolean estado) {
        System.out.println("clases.Datos.cantEmpleados()");
        try {
            setSql("SELECT COUNT(idEmpleado) "
                    + "FROM T_Empleados "
                    + "WHERE ESTADO IS " + estado + ";");
            PreparedStatement st = getConn().prepareStatement(getSql(),
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 1;
    }

    public synchronized int cantHorarios(boolean estado) {
        System.out.println("clases.Datos.cantHorarios()");
        try {
            setSql("SELECT COUNT(idHorario) "
                    + "FROM T_Horarios "
                    + "WHERE ESTADO IS " + estado + ";");
            PreparedStatement st = getConn().prepareStatement(getSql(),
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 1;
    }

    /**
     * Verificamos si existe la cedula del paciente antes de realizar un
     * registro a la base de datos.
     *
     * @param cedula Es el identificador unico de cada persona cuando nace.
     * @param estado Para indicar si el empleado esta activo o inactivo.
     * @return boolean si es verdadero el documento existe false puede
     * realizarse el registro a la base de datos.
     */
    public synchronized boolean existeEmpleado(String cedula) {
        System.out.println("clases.Datos.existePadre()");
        try {
            setSql("SELECT (1) "
                    + "FROM T_EMPLEADOS "
                    + "WHERE cedula = '" + cedula + "' and ESTADO IS false");
            PreparedStatement st = getConn().prepareStatement(getSql(),
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);
            ResultSet rs = st.executeQuery();
            return rs.next();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public synchronized boolean existeHorario(String nombre) {
        System.out.println("clases.Datos.existeFoto()");
        nombre = nombre.toUpperCase();
        setSql("SELECT (1) "
                + "FROM T_HORARIOS h "
                + "WHERE upper(h.descripcion) = '"
                + nombre + "'");
        try {
            PreparedStatement st = getConn().prepareStatement(getSql(),
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);
            ResultSet rs = st.executeQuery();
            return rs.next();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public synchronized ResultSet getEmpleadoEstado(boolean estado) {
        System.out.println("clases.Datos.getEmpleadoEstado()");
        try {
            setSql("SELECT IDEMPLEADO, CEDULA, NOMBRES, APELLIDOS, SEXO, "//5
                    + "coalesce(TELEFONO,'Sin Numero') as TELEFONO, "//6
                    + "coalesce(CARGO, 'Sin Cargo') as Cargo, "//7
                    + "coalesce(FECHANACIMIENTO, '01.01.00') as FECHANACIMIENTO, "//8
                    + "coalesce(CIUDAD, 'Sin Ciudad') as CIUDAD, "//9
                    + "coalesce(DIRECCION, 'Sin Direccion') as Direccion, "//10
                    + "coalesce(CORREO, 'Sin Correo') as CORREO "//11
                    + "FROM T_EMPLEADOS "
                    + "WHERE ESTADO IS " + estado + ";");
            PreparedStatement st = getConn().prepareStatement(getSql(),
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);
            return st.executeQuery();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized ResultSet getHorariosDisponibles(int idHorarios) {
        System.out.println("clases.Datos.getHorarios()");
        try {
            setSql("select a.idempleado, a.nombres||' '||a.apellidos as fullName, a.cargo \n"
                    + "from t_empleados a \n"
                    + "left join  t_asignacionhorarios b \n"
                    + "on a.idempleado = b.idempleado \n"
                    + "where b.idhorario is null or b.idhorario != " + idHorarios);
            PreparedStatement st = getConn().prepareStatement(getSql(),
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);
            return st.executeQuery();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized ResultSet getHorariosIncluidos(int idHorarios) {
        System.out.println("clases.Datos.getHorarios()");
        try {
            setSql("select a.idempleado, a.nombres||' '||a.apellidos as fullName, a.cargo \n"
                    + "from t_empleados a\n"
                    + "left join  t_asignacionhorarios b\n"
                    + "on a.idempleado = b.idempleado\n"
                    + "where b.idhorario = " + idHorarios);
            PreparedStatement st = getConn().prepareStatement(getSql(),
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);
            return st.executeQuery();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized ResultSet getHorariosEstado(boolean estado) {//Metodo Verificado
        System.out.println("clases.Datos.getHorariosEstado()");
        try {
            setSql("SELECT idhorario, descripcion, ingreso, almuerzo, "
                    + "reingreso, salida, tolerancia, estado "
                    + "FROM T_HORARIOS "
                    + "WHERE ESTADO IS " + estado + ";");
            PreparedStatement st = getConn().prepareStatement(getSql(),
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);
            return st.executeQuery();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized ResultSet getEmpleadoRecuperar(String cedula) {//Metodo verificado
        System.out.println("clases.Datos.getPadresEstadoID()");
        try {
            setSql("SELECT NOMBRES, APELLIDOS, SEXO, TELEFONO, "
                    + "CARGO, FECHANACIMIENTO, CIUDAD, DIRECCION, HUELLA, FOTO, "
                    + "ESTADO "
                    + "FROM T_Empleados "
                    + "WHERE CEDULA = '" + cedula + "' AND ESTADO IS FALSE;");
            PreparedStatement st = getConn().prepareStatement(getSql(),
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);
            return st.executeQuery();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized ResultSet getHorarios(boolean e) {//Metodo verificado
        System.out.println("clases.Datos.getHorarios()");
        try {
            setSql("SELECT IDHORARIO, DESCRIPCION "
                    + "FROM T_HORARIOS "
                    + "WHERE ESTADO IS " + e);
            PreparedStatement st = getConn().prepareStatement(getSql(),
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);
            return st.executeQuery();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized ResultSet getPathFoto(String cedula) {//Metodo verificado
        System.out.println("clases.Datos.getHorarios()");
        try {
            setSql("select IDFOTO, CEDULA, FECHA_ING, NOMBRE_ARCHIVO " +
                    "from T_FOTO_EMPLEADOS_SEL " +
                    "where Cedula  like '"+cedula+"' and NOMBRE_ARCHIVO is not null " + 
                            "order by 1 desc");
            PreparedStatement st = getConn().prepareStatement(getSql(),
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);
            return st.executeQuery();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized String ejecutarSQL(String sql) {
        System.out.println("clases.Datos.ejecutarSQL()");
        setSql(sql);
        try {//Insertamos en la tablas no a la vista...
            PreparedStatement st = getConn().prepareStatement(getSql(),
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);
            st.executeUpdate();
            return "Cambios Guardados";
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "No se pueden guardar los cambios";
        }
    }

    public synchronized String agregarHorario(Horarios h) {
        System.out.println("clases.Datos.agregarHorario()");
        try {
            setSql("update or insert into T_HORARIOS (IDHORARIO, DESCRIPCION, "
                    + "INGRESO, ALMUERZO, REINGRESO, SALIDA, TOLERANCIA, ESTADO) "
                    + "values (?, ?, ?, ?, ?, ?, ?, ?) "
                    + "matching(IDHORARIO)");
            try (PreparedStatement st = getConn().prepareStatement(getSql(),
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT)) {
                if (h.getIdHorario() != null) {
                    st.setInt(1, h.getIdHorario());
                } else {
                    st.setNull(1, java.sql.Types.INTEGER);
                }
                st.setString(2, h.getDescripcion());
                st.setTime(3, h.getIngreso());
                st.setTime(4, h.getAlmuerzo());
                st.setTime(5, h.getReingreso());
                st.setTime(6, h.getSalida());
                st.setInt(7, h.getTolerancia());
                st.setBoolean(8, h.isEstado());

                st.executeUpdate();
                st.close();
            }
            return "Horario agregado/actualizado correctamente!";
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al agregar horario, intente de nuevo!";
        }
    }

    public synchronized void agregarAsignacionHorario(Integer e, int h) {
        System.out.println("clases.Datos.agregarAsignacion()");
        try {
            setSql("update or insert into T_ASIGNACIONHORARIOS (idEmpleado, idHorario) "
                    + "values (?, ?) "
                    + "matching(idEmpleado)");
            try (PreparedStatement st = getConn().prepareStatement(getSql(),
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT)) {
                st.setInt(1, e);
                st.setInt(2, h);

                st.executeUpdate();
                st.close();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public synchronized String agregarEmpleado(Empleado e, boolean foto,
            boolean huella, boolean borrarFoto, boolean borrarHuella) {//Metodo Verificado
        System.out.println(
                Colores.ANSI_BLACK_BACKGROUND
                + Colores.ANSI_BLUE
                + "clases.Datos.agregarEmpleado"
                + Colores.ANSI_RESET);
        try {
            setSql("update or insert into T_EMPLEADOS_ins (CEDULA, NOMBRES, "
                    + "APELLIDOS, SEXO, TELEFONO, CARGO, FECHANACIMIENTO, "
                    + "CIUDAD, DIRECCION, ESTADO, CORREO) "
                    + "values (?,?,?,?,?,?,'" + e.getFechaNacimiento() + "',?,?,?,?) "
                    + "matching(CEDULA)");

            PreparedStatement st = getConn().prepareStatement(getSql());
            st.setString(1, e.getCedula().trim());
            st.setString(2, e.getNombres().trim());
            st.setString(3, e.getApellidos().trim());
            st.setString(4, "" + e.getSexo());
            st.setString(5, (e.getTelefono() == null ? null : e.getTelefono().trim()));
            st.setString(6, e.getCargo().trim());
            st.setString(7, e.getCiudad());
            st.setString(8, e.getDireccion());
            st.setBoolean(9, e.isEstado());
            st.setString(10, e.getCorreo());
            st.executeUpdate();

            if (foto) {
                setSql("insert into T_FOTOS_EMPLEADOS (Cedula, FOTO, NOMBRE_ARCHIVO) "
                        + "values (?,?,?) ");
                st = getConn().prepareStatement(getSql());
                String imageDataString = null;
                try {
                    if (e.getFoto() != null) {
                        FileInputStream imageInFile = new FileInputStream(e.getFoto());
                        byte imageData[] = new byte[(int) e.getFoto().length()];
                        imageInFile.read(imageData);
                        // Converting Image byte array into Base64 String
                        imageDataString = Base64.encodeBase64URLSafeString(imageData);
                    }
                } catch (FileNotFoundException ex) {
                    LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
                }
                st.setString(1, e.getCedula().trim());
                st.setString(2, imageDataString);
                st.setString(3, e.getFoto().getName());
                st.executeUpdate();
            }
            
            
            if (huella) {
                setSql("update or insert into T_EMPLEADOS_INS (Cedula, Huella) "
                        + "values (?,?) "
                        + "matching(CEDULA)");
                st = getConn().prepareStatement(getSql());
                st.setString(1, e.getCedula().trim());
                st.setBinaryStream(2, e.getHuella(), e.getTamHuella());
                st.executeUpdate();
            }

            //En dado caso que el empleado desea eliminar la huella del sistema...
            if (borrarHuella) {
                setSql("update T_EMPLEADOS_INS\n"
                        + "  set HUELLA = null\n"
                        + "  where CEDULA = '" + e.getCedula() + "'");
                st = getConn().prepareStatement(getSql());
                st.executeUpdate();
            }
            
            if(borrarFoto){
                setSql("insert into T_FOTOS_EMPLEADOS (CEDULA) values ('"+
                        e.getCedula()+"')");
                st = getConn().prepareStatement(getSql());
                st.executeUpdate();
            }

            String r = "Empleado actualizado correctamente";
            if (st.getUpdateCount() == -1) {
                r = "Empleado agregado correctamente";
            }
            return r;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al insertar Padre...";
        }
    }

    public synchronized String borrarEmpleado(String cedula) {//Metodo Verificado
        System.out.println("clases.Datos.borrarPadre()");
        try {
            setSql("DELETE from T_EMPLEADOS "
                    + "WHERE "
                    + " CEDULA = '" + cedula + "'");
            PreparedStatement st = getConn().prepareStatement(getSql(),
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);
            st.executeUpdate();
            return "Borrado correctamente";
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al borrar Empleado...";
        }
    }

    public synchronized String borrarHorario(int idHorario) {//Metodo Verificado
        System.out.println("clases.Datos.borrarPadre()");
        try {
            setSql("DELETE from T_Horarios "
                    + "WHERE "
                    + " idHorario = " + idHorario);
            PreparedStatement st = getConn().prepareStatement(getSql(),
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);
            st.executeUpdate();
            return "Horario borrado correctamente";
        } catch (SQLException ex) {
            if (ex.getMessage().contains("335544466")) {
                return "Horario palmado con unas asignaciones";
            }
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al borrar Horario...";
        }
    }

    public synchronized void borrarAsignacionEmpleado(int e) {//Metodo Verificado
        System.out.println("clases.Datos.borrarPadre()");
        try {
            setSql("DELETE from T_Asignacionhorarios "
                    + "WHERE "
                    + " idEmpleado = " + e);
            PreparedStatement st = getConn().prepareStatement(getSql(),
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);
            st.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public synchronized String guardarImagen(File file, String id, String query) {//Metodo Verificado
        System.out.println("clases.Datos.guardarImagen()");
// Reading a Image file from file system
        FileInputStream imageInFile = null;
        String imageDataString = null;
        try {
            if (file != null) {
                imageInFile = new FileInputStream(file);
                byte imageData[] = new byte[(int) file.length()];
                imageInFile.read(imageData);
                // Converting Image byte array into Base64 String
                imageDataString = Base64.encodeBase64URLSafeString(imageData);
            }
            PreparedStatement ps = null;
            ps = getConn().prepareStatement(query);
            ps.setString(1, id);
            ps.setString(2, imageDataString);

            ps.executeUpdate();

            return "Foto Insertada";
        } catch (FileNotFoundException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "Foto NO Insertada";
    }

    public synchronized Image getImagenes(String sql) {//Metodo Verificado
        System.out.println("clases.Datos.getImagenes()");
        try {
            PreparedStatement st = getConn().prepareStatement(sql,
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);
            ResultSet rs = st.executeQuery();
            byte[] data = null;
            if (rs.next()) {
                data = Base64.decodeBase64(rs.getString(1));
            } else {
                if (rs.next()) {
                    data = Base64.decodeBase64(rs.getString(1));
                }
            }

            if (data == null) {
                return null;
            }

            BufferedImage img = null;
            try {
                img = ImageIO.read(new ByteArrayInputStream(data));
            } catch (IOException ex) {
                LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            }
            return img;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public synchronized boolean tieneHuella(String cedula) {//Metodo Verificado
        System.out.println("clases.Datos.getHuella()");
        setSql("select (1) "
                + "from T_EMPLEADOS "
                + "where Cedula = '" + cedula + "' and huella is not null;");;

        boolean tiene;
        try {
            PreparedStatement st = getConn().prepareStatement(getSql(),
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);
            ResultSet rs = st.executeQuery();
            tiene = rs.next();
            System.out.println(tiene ? "Tiene huella" : "No Tiene Huella");
            return tiene;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("No Tiene Huella");
            return false;
        }

    }
}
