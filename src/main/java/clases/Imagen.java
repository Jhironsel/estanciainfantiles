
package clases;

import java.awt.image.BufferedImage;


public class Imagen {
    BufferedImage imagen;
    String nombre;
 
    public void setImagen(BufferedImage imagen) {
        this.imagen = imagen;
    }
 
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
 
    public BufferedImage getImagen() {
        return imagen;
    }
 
    public String getNombre() {
        return nombre;
    }

}