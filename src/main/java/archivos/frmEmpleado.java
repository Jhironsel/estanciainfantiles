package archivos;

import Clases.Colores;
import clases.Categoria;
import static clases.Datos.getDatos;
import clases.PanelConFondo;
import clases.TextPrompt;
import com.digitalpersona.onetouch.DPFPDataPurpose;
import com.digitalpersona.onetouch.DPFPFeatureSet;
import com.digitalpersona.onetouch.DPFPGlobal;
import com.digitalpersona.onetouch.DPFPSample;
import com.digitalpersona.onetouch.DPFPTemplate;
import com.digitalpersona.onetouch.capture.DPFPCapture;
import com.digitalpersona.onetouch.capture.event.DPFPDataAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPDataEvent;
import com.digitalpersona.onetouch.capture.event.DPFPErrorAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPErrorEvent;
import com.digitalpersona.onetouch.capture.event.DPFPReaderStatusAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPReaderStatusEvent;
import com.digitalpersona.onetouch.capture.event.DPFPSensorAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPSensorEvent;
import com.digitalpersona.onetouch.processing.DPFPEnrollment;
import com.digitalpersona.onetouch.processing.DPFPFeatureExtraction;
import com.digitalpersona.onetouch.processing.DPFPImageQualityException;
import com.digitalpersona.onetouch.verification.DPFPVerification;
import com.digitalpersona.onetouch.verification.DPFPVerificationResult;
import com.toedter.calendar.JTextFieldDateEditor;
import java.awt.Color;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import utilidades.Utilidades;
import static utilidades.Utilidades.LOGGER;
import static utilidades.Utilidades.adjustColumnPreferredWidths;
import utilidades.frmImagen;

public class frmEmpleado extends javax.swing.JInternalFrame {

    private static frmEmpleado empleado;
    private static int cliEmpleados;
    private int cantEmpleado;
    private boolean nuevo, foto = false, huella = false, borrarHuella = false,
            borrarFoto = false;
    private DefaultTableModel dtmEmpleado;
    private JTextFieldDateEditor editor;
    private final JButton dchFechaNacimientoButton;
    private TableRowSorter<TableModel> modeloOrdenado;
    private Map<String, Image> fotoMap;
    private String componente;
    private File fichero;
    //Varible que permite iniciar el dispositivo de lector de huella conectado
    // con sus distintos metodos.
    private final DPFPCapture lector = DPFPGlobal.getCaptureFactory().createCapture();
    private final DPFPEnrollment Reclutador = DPFPGlobal.getEnrollmentFactory().createEnrollment();
    //Esta variable tambien captura una huella del lector y crea sus caracteristcas para auntetificarla
    // o verificarla con alguna guardada en la BD
    private final DPFPVerification Verificador = DPFPGlobal.getVerificationFactory().createVerification();
    //Variable que para crear el template de la huella luego de que se hallan creado las caracteriticas
    // necesarias de la huella si no ha ocurrido ningun problema
    private DPFPTemplate template;
    private DPFPFeatureSet featuresinscripcion, featuresverificacion;

    public DPFPTemplate getPlantilla() {
        System.out.println("archivos.frmEmpleado.getPlantilla()");
        return template;
    }//Codigo Revisado 02/01/2020

    public void setPlantilla(DPFPTemplate newPlantilla) {
        System.out.println("archivos.frmEmpleado.setPlantilla()");
        DPFPTemplate oldPlantilla = this.template;
        this.template = newPlantilla;
        firePropertyChange("template", oldPlantilla, newPlantilla);
    }//Codigo Revisado 02/01/2020

    public frmEmpleado() {
        System.out.println("archivos.frmEmpleado.<init>()");
        initComponents();
        fotoMap = new HashMap<>();
        //dchFechaNamiento Personalizar
        editor = (JTextFieldDateEditor) dchFechaNacimiento.getDateEditor();

        dchFechaNacimientoButton = dchFechaNacimiento.getCalendarButton();
        editor.setEditable(false);
        dchFechaNacimientoButton.setEnabled(false);

        dchFechaNacimiento.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals("date")
                        && editor.isEditable()) {
                    cbSexo.requestFocusInWindow();
                    cbSexo.showPopup();
                }
            }
        });//Tomando control del dchFechaNamiento

        cantEmpleado = getDatos("Obtener la cantidad de empleados 1").
                cantEmpleados(cbEstadoEmpleados.isSelected());

        new TextPrompt("Filtro de busqueda", txtFiltroBusqueda);

        datosDetalles(false);
    }//Codigo Revisado 02/01/2020

    public synchronized static frmEmpleado getEmpleados() {
        System.out.println("archivos.frmEmpleado.getEmpleados()");
        if (empleado == null) {
            empleado = new frmEmpleado();
        }
        return empleado;
    }//Codigo Revisado 02/01/2020

    public void setCliEmpleado(int cliEmpleado) {
        System.out.println("archivos.frmEmpleado.setCliEmpleado()");
        frmEmpleado.cliEmpleados = cliEmpleado;
    }//Codigo Revisado 02/01/2020

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        miJPopUpMenu = new javax.swing.JPopupMenu();
        jmSeleccionarTodo1 = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jmCopia = new javax.swing.JMenuItem();
        jmCortar = new javax.swing.JMenuItem();
        jmPegar = new javax.swing.JMenuItem();
        jpmVistaPrevia = new javax.swing.JPopupMenu();
        jmiVistaPrevia = new javax.swing.JMenuItem();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel3 = new javax.swing.JPanel();
        jpDatos = new javax.swing.JPanel();
        cbSexo = new javax.swing.JComboBox();
        dchFechaNacimiento = new com.toedter.calendar.JDateChooser();
        txtNombres = new javax.swing.JTextField();
        txtApellidos = new javax.swing.JTextField();
        txtCargo = new javax.swing.JTextField();
        txtCiudad = new javax.swing.JTextField();
        txtDireccion = new javax.swing.JTextField();
        cbEstado = new javax.swing.JCheckBox();
        txtCedula = new javax.swing.JFormattedTextField();
        txtTelefono = new javax.swing.JFormattedTextField();
        btnValidaCedulaEmpleado = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jlCedula = new javax.swing.JLabel();
        jlNombres = new javax.swing.JLabel();
        jlApellidos = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jlFoto = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtCorreo = new javax.swing.JTextField();
        panBtns = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jspArea = new javax.swing.JScrollPane();
        txtArea = new javax.swing.JTextPane();
        jpHuella = new javax.swing.JPanel();
        jlImagenHuella = new javax.swing.JLabel();
        btnAgregarFoto = new javax.swing.JButton();
        btnBorrarFoto = new javax.swing.JButton();
        btnBorrarHuella = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jpDetalles = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtEmpleados = new JTable(){
            @Override
            public boolean isCellEditable(int rowIndex, int colIndex) { 
                return false; //Las celdas no son editables. 
            }
        };
        jpFiltro = new javax.swing.JPanel();
        cbEstadoEmpleados = new javax.swing.JCheckBox();
        txtFiltroBusqueda = new javax.swing.JTextField();
        jpNavegacion = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        btnPrimero = new javax.swing.JButton();
        btnAnterior = new javax.swing.JButton();
        btnSiguiente = new javax.swing.JButton();
        btnUltimo = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        btnNuevo = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnBorrar = new javax.swing.JButton();

        jmSeleccionarTodo1.setText("Seleccionar Todo");
        jmSeleccionarTodo1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmSeleccionarTodo1ActionPerformed(evt);
            }
        });
        miJPopUpMenu.add(jmSeleccionarTodo1);
        miJPopUpMenu.add(jSeparator2);

        jmCopia.setText("Copiar");
        jmCopia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmCopiaActionPerformed(evt);
            }
        });
        miJPopUpMenu.add(jmCopia);

        jmCortar.setText("Cortar");
        jmCortar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmCortarActionPerformed(evt);
            }
        });
        miJPopUpMenu.add(jmCortar);

        jmPegar.setText("Pegar");
        jmPegar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmPegarActionPerformed(evt);
            }
        });
        miJPopUpMenu.add(jmPegar);

        jmiVistaPrevia.setText("Vista Previa");
        jmiVistaPrevia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiVistaPreviaActionPerformed(evt);
            }
        });
        jpmVistaPrevia.add(jmiVistaPrevia);

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Mantenimiento de empleados");
        setMinimumSize(new java.awt.Dimension(0, 0));
        setNormalBounds(null);
        setPreferredSize(new java.awt.Dimension(824, 662));
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosed(evt);
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        jpDatos.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos"));
        jpDatos.setMaximumSize(new java.awt.Dimension(697, 450));
        jpDatos.setMinimumSize(new java.awt.Dimension(0, 0));
        jpDatos.setName(""); // NOI18N

        cbSexo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecccione Sexo", "Masculino", "Femenina" }));
        cbSexo.setToolTipText("Seleccione el sexo del empleado.");
        cbSexo.setEnabled(false);
        cbSexo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbSexoActionPerformed(evt);
            }
        });

        dchFechaNacimiento.setToolTipText("Ingrese fecha nacimiento");
        dchFechaNacimiento.setDateFormatString("dd.MM.yyyy");

        txtNombres.setEditable(false);
        txtNombres.setToolTipText("Inserte el nombre del Empleado.");
        txtNombres.setComponentPopupMenu(miJPopUpMenu);
        txtNombres.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNombresFocusLost(evt);
            }
        });
        txtNombres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombresActionPerformed(evt);
            }
        });

        txtApellidos.setEditable(false);
        txtApellidos.setToolTipText("Inserte los apellidos del empleado");
        txtApellidos.setComponentPopupMenu(miJPopUpMenu);
        txtApellidos.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtApellidosFocusLost(evt);
            }
        });
        txtApellidos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtApellidosActionPerformed(evt);
            }
        });

        txtCargo.setEditable(false);
        txtCargo.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtCargo.setToolTipText("Cargo del empleado en la organización");
        txtCargo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCargoActionPerformed(evt);
            }
        });

        txtCiudad.setEditable(false);
        txtCiudad.setToolTipText("Ingrese una ciudad");
        txtCiudad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCiudadActionPerformed(evt);
            }
        });

        txtDireccion.setEditable(false);
        txtDireccion.setToolTipText("Direccion para poder localizarlo.");
        txtDireccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDireccionActionPerformed(evt);
            }
        });

        cbEstado.setSelected(true);
        cbEstado.setText("Activo");
        cbEstado.setToolTipText("Estado del empleado en el sistema.");
        cbEstado.setEnabled(false);

        txtCedula.setEditable(false);
        try {
            txtCedula.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###-#######-#")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCedula.setText("000-0000000-0");
        txtCedula.setToolTipText("Inserte la cedula del Empleado, para proceder al ingreso al sistema");
        txtCedula.setComponentPopupMenu(miJPopUpMenu);
        txtCedula.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        txtCedula.setMaximumSize(new java.awt.Dimension(120, 27));
        txtCedula.setMinimumSize(new java.awt.Dimension(120, 27));
        txtCedula.setPreferredSize(new java.awt.Dimension(120, 27));
        txtCedula.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtCedulaFocusLost(evt);
            }
        });
        txtCedula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCedulaActionPerformed(evt);
            }
        });

        txtTelefono.setEditable(false);
        try {
            txtTelefono.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(###) ###-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtTelefono.setText("0000000000");
        txtTelefono.setToolTipText("Proporcione un numero de contacto.");
        txtTelefono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTelefonoActionPerformed(evt);
            }
        });

        btnValidaCedulaEmpleado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8-casilla-de-verificación-2-32.png"))); // NOI18N
        btnValidaCedulaEmpleado.setEnabled(false);
        btnValidaCedulaEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnValidaCedulaEmpleadoActionPerformed(evt);
            }
        });

        jLabel10.setText("Ciudad");

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel9.setText("Dirección");

        jLabel8.setText("Cargo");

        jLabel4.setText("Sexo");

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel7.setText("Telefono");

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel11.setText("Nacimiento");

        jlCedula.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jlCedula.setText("Cedula");

        jlNombres.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jlNombres.setText("Nombres");

        jlApellidos.setText("Apellidos");

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Foto del empleado", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        jPanel7.setMinimumSize(new java.awt.Dimension(0, 0));

        jlFoto.setForeground(new java.awt.Color(153, 153, 153));
        jlFoto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlFoto.setText("Click para agregar foto");
        jlFoto.setToolTipText("Click para buscar imagen o tomar una foto.");
        jlFoto.setAlignmentY(0.0F);
        jlFoto.setComponentPopupMenu(jpmVistaPrevia);
        jlFoto.setIconTextGap(0);
        jlFoto.setMaximumSize(new java.awt.Dimension(32767, 32767));
        jlFoto.setMinimumSize(new java.awt.Dimension(0, 0));
        jlFoto.setPreferredSize(null);
        jlFoto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlFotoMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jlFoto, javax.swing.GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addComponent(jlFoto, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        jLabel12.setText("Correo");

        txtCorreo.setEditable(false);
        txtCorreo.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtCorreo.setToolTipText("Correo de contacto, Valor unico por empleado");
        txtCorreo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCorreoActionPerformed(evt);
            }
        });

        panBtns.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Acciones", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        panBtns.setPreferredSize(new java.awt.Dimension(400, 190));
        panBtns.setLayout(null);

        jspArea.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        txtArea.setBackground(new java.awt.Color(204, 204, 204));
        txtArea.setSelectedTextColor(new java.awt.Color(153, 153, 153));
        jspArea.setViewportView(txtArea);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addComponent(jspArea, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jspArea, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );

        panBtns.add(jPanel8);
        jPanel8.setBounds(145, 30, 400, 110);

        jpHuella.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Huella Digital", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("sansserif", 0, 12))); // NOI18N
        jpHuella.setPreferredSize(new java.awt.Dimension(400, 270));
        jpHuella.setLayout(null);

        jlImagenHuella.setPreferredSize(null);
        jlImagenHuella.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlImagenHuellaMouseClicked(evt);
            }
        });
        jpHuella.add(jlImagenHuella);
        jlImagenHuella.setBounds(7, 18, 122, 93);

        panBtns.add(jpHuella);
        jpHuella.setBounds(10, 20, 135, 120);

        btnAgregarFoto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconfinder_camera-add_83558.png"))); // NOI18N
        btnAgregarFoto.setText("Agregar Foto");
        btnAgregarFoto.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnAgregarFoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarFotoActionPerformed(evt);
            }
        });

        btnBorrarFoto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Borrar 32 x 32.png"))); // NOI18N
        btnBorrarFoto.setText("Borrar foto");
        btnBorrarFoto.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnBorrarFoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarFotoActionPerformed(evt);
            }
        });

        btnBorrarHuella.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Borrar 32 x 32.png"))); // NOI18N
        btnBorrarHuella.setText("Borrar huella");
        btnBorrarHuella.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnBorrarHuella.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarHuellaActionPerformed(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_Admin_42px_1.png"))); // NOI18N

        javax.swing.GroupLayout jpDatosLayout = new javax.swing.GroupLayout(jpDatos);
        jpDatos.setLayout(jpDatosLayout);
        jpDatosLayout.setHorizontalGroup(
            jpDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpDatosLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jpDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jpDatosLayout.createSequentialGroup()
                        .addGroup(jpDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpDatosLayout.createSequentialGroup()
                                .addComponent(jlCedula, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(cbEstado))
                            .addGroup(jpDatosLayout.createSequentialGroup()
                                .addComponent(txtCedula, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(btnValidaCedulaEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dchFechaNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10)
                            .addComponent(txtCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jpDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlNombres, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNombres, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jpDatosLayout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(90, 90, 90)
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jpDatosLayout.createSequentialGroup()
                                .addComponent(cbSexo, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(5, 5, 5)
                                .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(5, 5, 5)
                        .addGroup(jpDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlApellidos, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtApellidos, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCargo, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(panBtns, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(6, 6, 6)
                .addGroup(jpDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBorrarHuella, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jpDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jpDatosLayout.createSequentialGroup()
                            .addGroup(jpDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnAgregarFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnBorrarFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jpDatosLayout.setVerticalGroup(
            jpDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpDatosLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jpDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpDatosLayout.createSequentialGroup()
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jpDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpDatosLayout.createSequentialGroup()
                                .addGap(38, 38, 38)
                                .addComponent(btnBorrarFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jpDatosLayout.createSequentialGroup()
                                .addGroup(jpDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnAgregarFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(26, 26, 26)
                                .addComponent(btnBorrarHuella, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jpDatosLayout.createSequentialGroup()
                        .addGroup(jpDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlCedula, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jpDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCedula, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnValidaCedulaEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(dchFechaNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(txtCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jpDatosLayout.createSequentialGroup()
                        .addComponent(jlNombres, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(txtNombres, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addGroup(jpDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jpDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbSexo, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jpDatosLayout.createSequentialGroup()
                        .addComponent(jlApellidos, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(txtApellidos, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(txtCargo, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(txtCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(panBtns, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );

        jpDetalles.setBorder(javax.swing.BorderFactory.createTitledBorder("Detalles"));
        jpDetalles.setMinimumSize(new java.awt.Dimension(300, 250));

        jtEmpleados.setAutoCreateRowSorter(true);
        jtEmpleados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jtEmpleados.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jtEmpleados.setDoubleBuffered(true);
        jtEmpleados.setPreferredSize(null);
        jtEmpleados.getTableHeader().setReorderingAllowed(false);
        jtEmpleados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtEmpleadosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jtEmpleados);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        cbEstadoEmpleados.setSelected(true);
        cbEstadoEmpleados.setText("Empleados Activos");
        cbEstadoEmpleados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbEstadoEmpleadosActionPerformed(evt);
            }
        });

        txtFiltroBusqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFiltroBusquedaKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jpFiltroLayout = new javax.swing.GroupLayout(jpFiltro);
        jpFiltro.setLayout(jpFiltroLayout);
        jpFiltroLayout.setHorizontalGroup(
            jpFiltroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpFiltroLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(txtFiltroBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 372, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cbEstadoEmpleados)
                .addGap(0, 0, 0))
        );
        jpFiltroLayout.setVerticalGroup(
            jpFiltroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpFiltroLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jpFiltroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbEstadoEmpleados)
                    .addComponent(txtFiltroBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout jpDetallesLayout = new javax.swing.GroupLayout(jpDetalles);
        jpDetalles.setLayout(jpDetallesLayout);
        jpDetallesLayout.setHorizontalGroup(
            jpDetallesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpDetallesLayout.createSequentialGroup()
                .addGroup(jpDetallesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jpFiltro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        jpDetallesLayout.setVerticalGroup(
            jpDetallesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpDetallesLayout.createSequentialGroup()
                .addComponent(jpFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jpNavegacion.setBorder(javax.swing.BorderFactory.createTitledBorder("Controles"));

        jPanel5.setMinimumSize(new java.awt.Dimension(0, 0));
        jPanel5.setLayout(new java.awt.GridLayout(1, 4, 4, 0));

        btnPrimero.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnPrimero.setForeground(new java.awt.Color(1, 1, 1));
        btnPrimero.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Anterior 32 x 32.png"))); // NOI18N
        btnPrimero.setMnemonic('p');
        btnPrimero.setText("Primero");
        btnPrimero.setToolTipText("Va al Primer Registro");
        btnPrimero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrimeroActionPerformed(evt);
            }
        });
        jPanel5.add(btnPrimero);

        btnAnterior.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnAnterior.setForeground(new java.awt.Color(1, 1, 1));
        btnAnterior.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Flecha Izquierda 32 x 32.png"))); // NOI18N
        btnAnterior.setMnemonic('a');
        btnAnterior.setText("Anterior");
        btnAnterior.setToolTipText("Va al Anterior Registro");
        btnAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnteriorActionPerformed(evt);
            }
        });
        jPanel5.add(btnAnterior);

        btnSiguiente.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnSiguiente.setForeground(new java.awt.Color(1, 1, 1));
        btnSiguiente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Flecha Derecha 32 x 32.png"))); // NOI18N
        btnSiguiente.setMnemonic('s');
        btnSiguiente.setText("Siguiente");
        btnSiguiente.setToolTipText("Va al Siguiente Registro");
        btnSiguiente.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguienteActionPerformed(evt);
            }
        });
        jPanel5.add(btnSiguiente);

        btnUltimo.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnUltimo.setForeground(new java.awt.Color(1, 1, 1));
        btnUltimo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Siguiente 32 x 32.png"))); // NOI18N
        btnUltimo.setMnemonic('u');
        btnUltimo.setText("Ultimo");
        btnUltimo.setToolTipText("Va al Ultimo Registro");
        btnUltimo.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnUltimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUltimoActionPerformed(evt);
            }
        });
        jPanel5.add(btnUltimo);

        jPanel6.setMinimumSize(new java.awt.Dimension(0, 0));
        jPanel6.setLayout(new java.awt.GridLayout(1, 0, 4, 0));

        btnNuevo.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnNuevo.setForeground(new java.awt.Color(1, 1, 1));
        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Documento nuevo 32 x 32.png"))); // NOI18N
        btnNuevo.setMnemonic('n');
        btnNuevo.setText("Nuevo");
        btnNuevo.setToolTipText("Crear un nuevo Registro");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        jPanel6.add(btnNuevo);

        btnModificar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnModificar.setForeground(new java.awt.Color(1, 1, 1));
        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Editar Documento 32 x 32.png"))); // NOI18N
        btnModificar.setMnemonic('m');
        btnModificar.setText("Modificar");
        btnModificar.setToolTipText("Modificar Registro Actual");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });
        jPanel6.add(btnModificar);

        btnGuardar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnGuardar.setForeground(new java.awt.Color(1, 1, 1));
        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Guardar 32 x 32.png"))); // NOI18N
        btnGuardar.setMnemonic('g');
        btnGuardar.setText("Guardar");
        btnGuardar.setToolTipText("Guardar Registro Actual");
        btnGuardar.setEnabled(false);
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        jPanel6.add(btnGuardar);

        btnCancelar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(1, 1, 1));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Cancelar 32 x 32.png"))); // NOI18N
        btnCancelar.setMnemonic('c');
        btnCancelar.setText("Cancelar");
        btnCancelar.setToolTipText("Cancela la Operacion del Registro");
        btnCancelar.setEnabled(false);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        jPanel6.add(btnCancelar);

        btnBorrar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnBorrar.setForeground(new java.awt.Color(1, 1, 1));
        btnBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Borrar 32 x 32.png"))); // NOI18N
        btnBorrar.setMnemonic('b');
        btnBorrar.setText("Borrar");
        btnBorrar.setToolTipText("Borrar Registro Actual");
        btnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarActionPerformed(evt);
            }
        });
        jPanel6.add(btnBorrar);

        javax.swing.GroupLayout jpNavegacionLayout = new javax.swing.GroupLayout(jpNavegacion);
        jpNavegacion.setLayout(jpNavegacionLayout);
        jpNavegacionLayout.setHorizontalGroup(
            jpNavegacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jpNavegacionLayout.setVerticalGroup(
            jpNavegacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpNavegacionLayout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jpDatos, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jpNavegacion, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jpDetalles, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jpDatos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jpDetalles, javax.swing.GroupLayout.PREFERRED_SIZE, 223, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jpNavegacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jScrollPane2.setViewportView(jPanel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 812, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 689, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
//Codigo Revisado 02/01/2020
    private void btnPrimeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrimeroActionPerformed
        System.out.println("archivos.frmEmpleado.btnPrimeroActionPerformed()");
        cliEmpleados = 0;
        jtEmpleados.setRowSelectionInterval(cliEmpleados, cliEmpleados);
        btnPrimero.requestFocusInWindow();
    }//GEN-LAST:event_btnPrimeroActionPerformed

    private void btnAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnteriorActionPerformed
        System.out.println("archivos.frmEmpleado.btnAnteriorActionPerformed()");
        cliEmpleados--;
        if (cliEmpleados == -1) {
            cliEmpleados = cantEmpleado - 1;
        }
        jtEmpleados.setRowSelectionInterval(cliEmpleados, cliEmpleados);
        btnAnterior.requestFocusInWindow();
    }//GEN-LAST:event_btnAnteriorActionPerformed

    private void btnSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguienteActionPerformed
        System.out.println("archivos.frmEmpleado.btnSiguienteActionPerformed()");
        cliEmpleados++;
        if (cliEmpleados == cantEmpleado) {
            cliEmpleados = 0;
        }
        jtEmpleados.setRowSelectionInterval(cliEmpleados, cliEmpleados);
        btnSiguiente.requestFocusInWindow();
    }//GEN-LAST:event_btnSiguienteActionPerformed

    private void btnUltimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUltimoActionPerformed
        System.out.println("archivos.frmEmpleado.btnUltimoActionPerformed()");
        cliEmpleados = cantEmpleado - 1;
        jtEmpleados.setRowSelectionInterval(cliEmpleados, cliEmpleados);
        btnUltimo.requestFocusInWindow();
    }//GEN-LAST:event_btnUltimoActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        System.out.println("archivos.frmEmpleado.btnNuevoActionPerformed()");
        //Activamos el Flag de registro Nuevo        
        nuevo = true;
        datosDetalles(true);
        limpiarCampos();
        navegador(false);
        controlesEditable(false);
        btnGuardar.setEnabled(false);
        btnValidaCedulaEmpleado.setEnabled(true);
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        System.out.println("archivos.frmEmpleado.btnModificarActionPerformed()");
        if (jtEmpleados.getRowCount() == 0) {
            JOptionPane.showInternalMessageDialog(this,
                    "No se encuentra datos en la tabla. Registre un empleado",
                    "Proceso de validacion de datos",
                    JOptionPane.DEFAULT_OPTION);
            return;
        }//Validamos que tenga registro la tabla
        if (jtEmpleados.getSelectedRow() == -1) {
            JOptionPane.showInternalMessageDialog(this,
                    "Debe seleccionar un registro de la tabla",
                    "Proceso de validacion de datos",
                    JOptionPane.DEFAULT_OPTION);
            jtEmpleados.requestFocusInWindow();
            return;
        }//Nos aseguramos que tengas un registro seleccionado
        txtArea.setText("");
        controlesEditable(true);//Trabajando
        datosDetalles(true);
        nuevo = false;
        mostrarRegistro();
        navegador(false);
        btnValidaCedulaEmpleado.setEnabled(false);
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        System.out.println("archivos.frmEmpleado.btnGuardarActionPerformed()");
        //Validaciones de datos...
        if (txtCedula.getValue() == null) {
            JOptionPane.showInternalConfirmDialog(this,
                    "Inserte la cedula!!!", "Olvida algo!",
                    JOptionPane.DEFAULT_OPTION);
            txtCedula.requestFocusInWindow();
            return;
        }

        if (txtNombres.getText().equals("")) {
            JOptionPane.showInternalConfirmDialog(this,
                    "Inserte el Nombre!!!", "Olvida algo!",
                    JOptionPane.DEFAULT_OPTION);
            txtNombres.requestFocusInWindow();
            return;
        }

        if (txtApellidos.getText().equals("")) {
            JOptionPane.showInternalConfirmDialog(this,
                    "Inserte el Apellido...", "Olvida algo!",
                    JOptionPane.DEFAULT_OPTION);
            txtApellidos.requestFocusInWindow();
            return;
        }

        if (txtTelefono.getValue() == null) {
            int opc = JOptionPane.showInternalConfirmDialog(this,
                    "¿Desea continuar sin telefono?",
                    "Confirmacion de procedimiento",
                    JOptionPane.YES_NO_OPTION);

            if (opc == JOptionPane.NO_OPTION) {
                txtTelefono.requestFocusInWindow();
                return;
            }
        }

        if (dchFechaNacimiento.getDate() == null) {
            JOptionPane.showInternalConfirmDialog(this,
                    "Debe indicar una fecha de nacimiento...",
                    "Olvida algo!", JOptionPane.DEFAULT_OPTION);
            dchFechaNacimiento.requestFocusInWindow();
            return;
        }

        if (dchFechaNacimiento.getDate().after(new Date())) {
            JOptionPane.showInternalConfirmDialog(this,
                    "La Fecha de Nacimiento debe ser Anterior a la Fecha Actual",
                    "Olvida algo!", JOptionPane.DEFAULT_OPTION);
            dchFechaNacimiento.requestFocusInWindow();
            return;
        }

        if (!cbEstado.isSelected()) {
            int resp = JOptionPane.showInternalConfirmDialog(
                    this,
                    "Desea dejar empleado inactivo?",
                    "Proceso de validacion",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (resp == JOptionPane.NO_OPTION) {
                cbEstado.setSelected(true);
                return;
            }
        }
        if (!txtCorreo.getText().isEmpty()) {
            if (!Utilidades.validaCorreo(txtCorreo.getText())) {
                JOptionPane.showInternalMessageDialog(
                        this,
                        "Correo no valido!!!",
                        "Validacion de correo",
                        JOptionPane.ERROR_MESSAGE);
                txtCorreo.setText("");
                txtCorreo.requestFocusInWindow();
                return;
            }
        }//FIN de las Validaciones..........................
        
        ByteArrayInputStream temp = null;
        int tam = 0;
        if (template != null) {
            temp = new ByteArrayInputStream(template.serialize());
            tam = template.serialize().length;
        }
        Empleado e = new Empleado(
                nuevo ? -1 : ((Categoria) jtEmpleados.getValueAt(cliEmpleados, 0))
                                .getIdCategoria(),
                nuevo ? (String) txtCedula.getValue() : ((Categoria) txtCedula.getValue())
                        .getDescripcion(),
                txtNombres.getText(),
                txtApellidos.getText(),
                (cbSexo.getSelectedIndex() == 1 ? 'm' : 'f'),
                (String) txtTelefono.getValue(),
                txtCargo.getText(),
                Utilidades.formatDate(dchFechaNacimiento.getDate(), ""),
                txtCiudad.getText(),
                txtDireccion.getText(),
                cbEstado.isSelected(),
                temp,
                tam,
                fichero,
                txtCorreo.getText());

        JOptionPane.showInternalConfirmDialog(
                this,
                getDatos("Agregar empleado al sistema").
                        agregarEmpleado(e, foto, huella, borrarFoto, borrarHuella),
                "Error inesperado",
                JOptionPane.DEFAULT_OPTION);

        llenarTabla(cbEstadoEmpleados.isSelected());
        btnCancelarActionPerformed(null);
        cantEmpleado = getDatos("Obteniendo cantidad de empleados del sistema 2").
                cantEmpleados(cbEstadoEmpleados.isSelected());
        fichero = null;
        foto = false;
        borrarFoto = false;

        huella = false;
        borrarHuella = false;
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        System.out.println("archivos.frmEmpleado.btnCancelarActionPerformed()");
        //Desactivamos el Flag de registro Nuevo
        int resp = JOptionPane.showInternalConfirmDialog(this,
                "Desea cancelar el regiistro actual?",
                "Validacion de cancelacion!",
                JOptionPane.YES_NO_OPTION);

        if (JOptionPane.NO_OPTION == resp) {
            return;
        }
        nuevo = false;
        datosDetalles(false);
        navegador(true);
        btnPrimero.doClick();
        txtArea.setText("");
        stop();
        jlFoto.setIcon(null);//Problemas con el icono de la foto en el empleado
        //Cuando trato de ver la de otro registro me aparece la misma....

    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarActionPerformed
        System.out.println("archivos.frmEmpleado.btnBorrarActionPerformed()");
        if (jtEmpleados.getSelectedRow() == -1) {
            JOptionPane.showInternalMessageDialog(this,
                    "Debe de seleccionar un registro de la tabla!!!",
                    "Proceso de validación",
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        int resp = JOptionPane.showInternalConfirmDialog(this,
                "Desea borrar el empleado "
                + (String) jtEmpleados.getValueAt(cliEmpleados, 1) + " "
                + (String) jtEmpleados.getValueAt(cliEmpleados, 2)
                + "\nDesea Continuar?",
                "Proceso de validacion de registros",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.INFORMATION_MESSAGE);
        if (resp == JOptionPane.NO_OPTION) {
            return;
        }
        getDatos("Borrando empleado del sistema").borrarEmpleado(
                ((Categoria) jtEmpleados.getValueAt(cliEmpleados, 0)).
                        getDescripcion().trim());
        cliEmpleados = 0;
        //Actualizamos los cambios en la Tabla
        llenarTabla(cbEstadoEmpleados.isSelected());
        cantEmpleado = getDatos("Obteniendo la cantidad de empleados del sistema 3").
                cantEmpleados(cbEstadoEmpleados.isSelected());
    }//GEN-LAST:event_btnBorrarActionPerformed

    private void jtEmpleadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtEmpleadosMouseClicked
        System.out.println("archivos.frmEmpleado.jtEmpleadosMouseClicked()");
        cliEmpleados = jtEmpleados.getSelectedRow();
        if (evt.getClickCount() == 2) {
            btnModificarActionPerformed(null);
        }
    }//GEN-LAST:event_jtEmpleadosMouseClicked

    private void jmSeleccionarTodo1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmSeleccionarTodo1ActionPerformed
        System.out.println("archivos.frmEmpleado.jmSeleccionarTodo1ActionPerformed()");
        if (componente.equals("txtCedula")) {
            txtCedula.setSelectionStart(0);
            txtCedula.setSelectionEnd(txtCedula.getText().length());
        }
        if (componente.equals("txtNombres")) {
            txtNombres.setSelectionStart(0);
            txtNombres.setSelectionEnd(txtNombres.getText().length());
        }
        if (componente.equals("txtApellidos")) {
            txtApellidos.setSelectionStart(0);
            txtApellidos.setSelectionEnd(txtApellidos.getText().length());
        }

        componente = "";
    }//GEN-LAST:event_jmSeleccionarTodo1ActionPerformed

    private void jmCopiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmCopiaActionPerformed
        System.out.println("archivos.frmEmpleado.jmCopia1ActionPerformed()");
        if (componente.equals("txtCedula")) {
            txtCedula.copy();
        }
        if (componente.equals("txtNombres")) {
            txtNombres.copy();
        }
        if (componente.equals("txtApellidos")) {
            txtApellidos.copy();
        }
        componente = "";
    }//GEN-LAST:event_jmCopiaActionPerformed

    private void jmCortarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmCortarActionPerformed
        System.out.println("archivos.frmEmpleado.jmCortar1ActionPerformed()");
        if (componente.equals("txtCedula")) {
            txtCedula.cut();
        }
        if (componente.equals("txtNombres")) {
            txtNombres.cut();
        }
        if (componente.equals("txtApellidos")) {
            txtApellidos.cut();
        }
        componente = "";
    }//GEN-LAST:event_jmCortarActionPerformed

    private void jmPegarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmPegarActionPerformed
        System.out.println("archivos.frmEmpleado.jmPegar1ActionPerformed()");
        if (componente.equals("txtCedula")) {
            txtCedula.paste();
        }
        if (componente.equals("txtNombres")) {
            txtNombres.paste();
        }
        if (componente.equals("txtApellidos")) {
            txtApellidos.paste();
        }
        componente = "";
    }//GEN-LAST:event_jmPegarActionPerformed

    private void txtNombresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombresActionPerformed
        if (btnPrimero.isEnabled()) {
            return;
        }
        System.out.println("archivos.frmEmpleado.txtNombresActionPerformed()");
        txtApellidos.requestFocusInWindow();
        Utilidades.showTooltip(txtApellidos);
    }//GEN-LAST:event_txtNombresActionPerformed

    private void txtCedulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCedulaActionPerformed
        System.out.println("archivos.frmEmpleado.txtCedulaActionPerformed()");
        btnValidaCedulaEmpleado.requestFocusInWindow();
    }//GEN-LAST:event_txtCedulaActionPerformed

    private void txtApellidosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtApellidosActionPerformed
        System.out.println("archivos.frmEmpleado.txtApellidosActionPerformed()");
        dchFechaNacimientoButton.doClick();
        Utilidades.showTooltip(dchFechaNacimiento);
    }//GEN-LAST:event_txtApellidosActionPerformed

    private void txtTelefonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTelefonoActionPerformed
        System.out.println("archivos.frmEmpleado.txtTelefonoActionPerformed()");
        txtCargo.requestFocusInWindow();
    }//GEN-LAST:event_txtTelefonoActionPerformed

    private void txtDireccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDireccionActionPerformed
        if (btnPrimero.isEnabled()) {
            return;
        }
        System.out.println("archivos.frmEmpleado.txtDireccionActionPerformed()");
        txtCorreo.requestFocusInWindow();
        Utilidades.showTooltip(txtCorreo);
    }//GEN-LAST:event_txtDireccionActionPerformed

    private void txtCiudadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCiudadActionPerformed
        if (btnPrimero.isEnabled()) {
            return;
        }
        System.out.println("archivos.frmEmpleado.txtCiudadActionPerformed()");
        txtDireccion.requestFocusInWindow();
        Utilidades.showTooltip(txtDireccion);
    }//GEN-LAST:event_txtCiudadActionPerformed

    private void txtCargoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCargoActionPerformed
        if (btnPrimero.isEnabled()) {
            return;
        }
        System.out.println("archivos.frmEmpleado.txtCorreoActionPerformed()");
        txtCiudad.requestFocusInWindow();
        Utilidades.showTooltip(txtCiudad);
    }//GEN-LAST:event_txtCargoActionPerformed

    private void cbEstadoEmpleadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbEstadoEmpleadosActionPerformed
        if (!isShowing()) {
            return;
        }
        System.out.println("archivos.frmEmpleado.cbEmpleadosActivosActionPerformed()");
        if (cbEstadoEmpleados.isSelected()) {
            cbEstadoEmpleados.setText("Empleado Activos");
        } else {
            cbEstadoEmpleados.setText("Empleado Inactivos");
        }
        cantEmpleado = getDatos("Obteniendo la cantidad de empleados del sistema 4").
                cantEmpleados(cbEstadoEmpleados.isSelected());
        llenarTabla(cbEstadoEmpleados.isSelected());
    }//GEN-LAST:event_cbEstadoEmpleadosActionPerformed

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        System.out.println("archivos.frmEmpleado.formInternalFrameOpened()");
        iniciarLector();
        llenarTabla(true);
    }//GEN-LAST:event_formInternalFrameOpened

    private void cbSexoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbSexoActionPerformed
        System.out.println("archivos.frmEmpleado.cbSexoActionPerformed()");
        txtTelefono.requestFocusInWindow();
        Utilidades.showTooltip(txtTelefono);
    }//GEN-LAST:event_cbSexoActionPerformed

    private void btnValidaCedulaEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnValidaCedulaEmpleadoActionPerformed
        System.out.println("archivos.frmEmpleado.btnValidaCedulaEmpleadoActionPerformed()");
        if (txtCedula.getValue() == null) {
            JOptionPane.showInternalMessageDialog(this, "Cedula no valida");
            txtCedula.setValue(null);
            txtCedula.requestFocusInWindow();
            return;
        }

        for (int i = 0; i < jtEmpleados.getRowCount(); i++) {
            if (txtCedula.getValue().toString().equals(((Categoria) jtEmpleados.getValueAt(i, 0))
                    .getDescripcion())) {
                JOptionPane.showMessageDialog(
                        this,
                        "La cedula se encuentra registrada y activa",
                        "Procedo de validación",
                        JOptionPane.WARNING_MESSAGE);

                txtCedula.setValue(null);
                txtCedula.requestFocusInWindow();
                return;
            }
        }

        if (!Utilidades.validarCedula(txtCedula.getText())) {
            JOptionPane.showMessageDialog(this, "Cedula no es valida.: " + txtCedula.getText());
            txtCedula.setValue(null);
            txtCedula.requestFocusInWindow();
            return;
        }

        final SwingWorker w = new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                if (getDatos("Investigando cedula del empleado").existeEmpleado(
                        txtCedula.getValue().toString())) {
                    int opc = JOptionPane.showConfirmDialog(null,
                            "Este numero de cedula existe en el sistema.\n "
                            + "¿Desea recuperarlo?", "Recuperar cuenta?",
                            JOptionPane.YES_NO_OPTION);

                    if (opc == JOptionPane.YES_OPTION) {
                        ResultSet r = getDatos("Recuperar información del empleado."
                        ).getEmpleadoRecuperar(txtCedula.getValue().toString());
                        try {
                            r.next();
                            txtNombres.setText(r.getString("NOMBRES"));
                            txtApellidos.setText(r.getString("APELLIDOS"));
                            txtTelefono.setValue(r.getObject("TELEFONO"));
                            txtCargo.setText(r.getString("CARGO").trim());
                            txtDireccion.setText(r.getString("DIRECCION"));
                            txtCiudad.setText(r.getString("CIUDAD"));
                            dchFechaNacimiento.setDate(r.getDate("FECHANACIMIENTO"));

                            char sex = r.getString("SEXO").charAt(0);
                            String sexo = sex == 'm' ? "Masculino" : sex == 'f' ? "Femenina"
                                    : "Selecccione Sexo";
                            for (int i = 0; i < cbSexo.getItemCount(); i++) {
                                cbSexo.setSelectedIndex(i);
                                if (((String) cbSexo.getSelectedItem()).equals(sexo)) {
                                    break;
                                }
                            }

                            verificarFotoHuella(txtCedula.getText().trim());
                            txtCedula.setEditable(false);
                            controlesEditable(true);
                            btnGuardar.setEnabled(true);
                            return true;
                        } catch (SQLException ex) {
                            LOGGER.getLogger(frmEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {//De lo contrario limpio el campo y intento de nuevo.
                        txtCedula.setValue(null);
                        txtCedula.requestFocusInWindow();
                        return false;
                    }
                } else {
                    controlesEditable(true);
                    start();
                    estadoHuellas();
                    txtCedula.setEditable(false);
                    btnGuardar.setEnabled(true);
                    navegador(!nuevo);
                    txtNombres.requestFocusInWindow();
                    Utilidades.showTooltip(txtNombres);
                }
                return true;
            }
        };

        w.execute();
    }//GEN-LAST:event_btnValidaCedulaEmpleadoActionPerformed

    private void txtFiltroBusquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFiltroBusquedaKeyReleased
        System.out.println("archivos.frmEmpleado.txtFiltroBusquedaKeyReleased()");
        modeloOrdenado.setRowFilter(RowFilter.regexFilter(
                "(?i)" + txtFiltroBusqueda.getText(), 0, 1, 2, 3, 4, 5, 6, 7, 8,
                9));
    }//GEN-LAST:event_txtFiltroBusquedaKeyReleased

    private void jlFotoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlFotoMouseClicked

    }//GEN-LAST:event_jlFotoMouseClicked

    private void txtCorreoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCorreoActionPerformed
        if (btnPrimero.isEnabled()) {
            return;
        }
        System.out.println("archivos.frmEmpleado.txtDireccionActionPerformed()");
        btnGuardar.requestFocusInWindow();
        Utilidades.showTooltip(btnGuardar);
    }//GEN-LAST:event_txtCorreoActionPerformed

    private void txtNombresFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNombresFocusLost
        System.out.println("archivos.frmEmpleado.txtNombresFocusLost()");
        componente = "txtNombres";
    }//GEN-LAST:event_txtNombresFocusLost

    private void txtApellidosFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtApellidosFocusLost
        System.out.println("archivos.frmEmpleado.txtApellidosFocusLost()");
        componente = "txtApellidos";
    }//GEN-LAST:event_txtApellidosFocusLost

    private void txtCedulaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCedulaFocusLost
        System.out.println("archivos.frmEmpleado.txtCedulaFocusLost()");
        componente = "txtCedula";
    }//GEN-LAST:event_txtCedulaFocusLost

    private void formInternalFrameClosed(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosed
        stop();
    }//GEN-LAST:event_formInternalFrameClosed

    private void jlImagenHuellaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlImagenHuellaMouseClicked
        if (evt.getClickCount() == 2) {
            frmImagen obj = new frmImagen(null, true,
                    new ImageIcon(Utilidades.iconToImage(jlImagenHuella.getIcon())));
            obj.setLocationRelativeTo(null);
            obj.setVisible(true);
        }
    }//GEN-LAST:event_jlImagenHuellaMouseClicked

    private void btnBorrarFotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarFotoActionPerformed
        if (!btnBorrarFoto.isEnabled()) {
            return;
        }
        btnBorrarFoto.setEnabled(false);
        jlFoto.setText("Click para agregar foto");
        jlFoto.setIcon(null);

        foto = false;
        borrarFoto = true;

    }//GEN-LAST:event_btnBorrarFotoActionPerformed

    private void btnBorrarHuellaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarHuellaActionPerformed
        if (!btnBorrarHuella.isEnabled()) {
            return;
        }
        btnBorrarHuella.setEnabled(false);

        borrarHuella = true;

        start();
        estadoHuellas();
        gifDinamico();
    }//GEN-LAST:event_btnBorrarHuellaActionPerformed

    private void btnAgregarFotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarFotoActionPerformed
        System.out.println("archivos.frmEmpleado.jlFotoMouseClicked()");
        if (btnValidaCedulaEmpleado.isEnabled()) {
            JOptionPane.showMessageDialog(this,
                    "Debe validar una cedula!!!",
                    "Proceso de validación de empleado",
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        String[] opr = new String[]{
            "Seleccione una opcion",
            "Desde un Archivo",
            "Desde una Camara"};

        String resp = (String) JOptionPane.showInternalInputDialog(
                this,
                "Elija una acción para realizar",
                "Proceso de acciones",
                JOptionPane.QUESTION_MESSAGE,
                null,
                opr,
                opr[0]);

        if (resp == null) {
            return;
        }

        switch (resp) {
            case "Desde un Archivo":
                /*
                    El objeto img se inserta dentro del fc para mostrar las imagenes
                    cuando sean seleccionadas en el cuadro de FileChooser.
                 */
                JLabel img = new JLabel();
                img.setHorizontalAlignment(JLabel.CENTER);
                JFileChooser fc = new JFileChooser();
                fc.setFileFilter(new FileNameExtensionFilter("*.PNG, *.JPEG, *.JPG", "png",
                        "jpeg", "jpg"));
                fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
                fc.setAccessory(img);
                fc.addPropertyChangeListener((PropertyChangeEvent evt1) -> {
                    try {
                        if (evt1.getPropertyName().equals(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY)) {
                            img.setText("");
                            ImageIcon icon = new ImageIcon(fc.getSelectedFile().getPath());
                            img.setIcon(new ImageIcon(icon.getImage().getScaledInstance(200, 200,
                                    Image.SCALE_SMOOTH)));
                        }
                    } catch (Exception e) {
                        img.setText("Seleccionar Imagen");
                        img.setIcon(null);
                    }
                });
                int seleccion = fc.showDialog(this, "Tomar Imagen");
                if (seleccion == JFileChooser.APPROVE_OPTION) {
                    if (fc.getSelectedFile().getName().length() > 40) {
                        JOptionPane.showInternalMessageDialog(this,
                                "Renombre el archivo a menos de 40 caracteres.",
                                "Validación de proceso",
                                JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    fichero = fc.getSelectedFile();

                    ImageIcon icon = new ImageIcon(fichero.getAbsolutePath());

                    String cedula = "Empleado_" + txtCedula.getText().trim() + ".JPG";

                    File f = new File(fichero.getAbsolutePath());
                    for (int i = 1; true; i++) {
                        if (f.exists()) {
                            f = new File("foto/Empleado_" + cedula + "_" + i + ".JPG");
                        } else {
                            break;
                        }
                    }

                    try {
                        ImageIO.write(PanelConFondo.getBufferedImage(
                                icon.getImage(), jlFoto), "JPG", new File("foto/" + cedula));
                    } catch (IOException t) {
                        t.printStackTrace();
                    }

                    jlFoto.setText("");
                    jlFoto.setIcon(new ImageIcon(icon.getImage().getScaledInstance(
                            180, 180, Image.SCALE_SMOOTH)));

                    fotoMap.remove(txtCedula.getText().trim(), null);
                    foto = true;
                    borrarFoto = false;
                    btnBorrarFoto.setEnabled(true);
                }

                break;
            case "Desde una Camara":
                frmCamara c = new frmCamara(null, true, jlFoto,
                        txtCedula.getText().trim());
                c.setLocationRelativeTo(null);
                c.setVisible(true);
                if (c.isListo()) {
                    System.out.println("*********Entro a camara desde aqui**********");
                    fichero = new File(c.getRuta());
                    ImageIcon icon = new ImageIcon(fichero.getAbsolutePath());
                    jlFoto.setText("");
                    jlFoto.setIcon(new ImageIcon(icon.getImage().getScaledInstance(
                            180, 180, Image.SCALE_SMOOTH)));
                    fotoMap.remove(txtCedula.getText().trim(), null);
                    fotoMap.put(txtCedula.getText().trim(), icon.getImage());

                    foto = true;
                    borrarFoto = false;

                    btnBorrarFoto.setEnabled(true);
                    System.out.println("Fichero creado");
                } else {
                    c.dispose();
                }
                break;
        }
    }//GEN-LAST:event_btnAgregarFotoActionPerformed

    private void jmiVistaPreviaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiVistaPreviaActionPerformed
//        new ImageIcon("foto/Empleado_" + txtCedula.getText() + ".JPG").
//                getImage().getScaledInstance(600, 600, Image.SCALE_DEFAULT);
        System.out.println("Cedula: "+txtCedula.getText().replace("-", "").trim()+" Entoces es: "+txtCedula.getText().replace("-", "").trim().isEmpty());

        if(txtCedula.getText().replace("-", "").trim().isEmpty())
            return;
        frmImagen obj = new frmImagen(null, true, new ImageIcon(
                (fotoMap.containsKey(txtCedula.getText().trim())
                ? fotoMap.get(txtCedula.getText().trim()).
                        getScaledInstance(600, 600, Image.SCALE_DEFAULT)
                : getDatos("Buscar imagen del empleado a la base de datos").
                        getImagenes("select FOTO "
                                + "from T_EMPLEADOS "
                                + "where Cedula = '" + txtCedula.getText().trim() + "';"))));
        //Al tirar la foto y ver la vista previa la foto no esta aun ni en fotoMap
        //Ni en la base de datos....
        //La foto esta en la carbeta de foto...

        obj.setLocationRelativeTo(null);
        obj.setVisible(true);
    }//GEN-LAST:event_jmiVistaPreviaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarFoto;
    private javax.swing.JButton btnAnterior;
    private javax.swing.JButton btnBorrar;
    private javax.swing.JButton btnBorrarFoto;
    private javax.swing.JButton btnBorrarHuella;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnPrimero;
    private javax.swing.JButton btnSiguiente;
    private javax.swing.JButton btnUltimo;
    private javax.swing.JButton btnValidaCedulaEmpleado;
    private javax.swing.JCheckBox cbEstado;
    private javax.swing.JCheckBox cbEstadoEmpleados;
    private javax.swing.JComboBox cbSexo;
    private com.toedter.calendar.JDateChooser dchFechaNacimiento;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JLabel jlApellidos;
    private javax.swing.JLabel jlCedula;
    private javax.swing.JLabel jlFoto;
    private javax.swing.JLabel jlImagenHuella;
    private javax.swing.JLabel jlNombres;
    private javax.swing.JMenuItem jmCopia;
    private javax.swing.JMenuItem jmCortar;
    private javax.swing.JMenuItem jmPegar;
    private javax.swing.JMenuItem jmSeleccionarTodo1;
    private javax.swing.JMenuItem jmiVistaPrevia;
    private javax.swing.JPanel jpDatos;
    private javax.swing.JPanel jpDetalles;
    private javax.swing.JPanel jpFiltro;
    private javax.swing.JPanel jpHuella;
    private javax.swing.JPanel jpNavegacion;
    private javax.swing.JPopupMenu jpmVistaPrevia;
    private javax.swing.JScrollPane jspArea;
    private javax.swing.JTable jtEmpleados;
    private javax.swing.JPopupMenu miJPopUpMenu;
    private javax.swing.JPanel panBtns;
    private javax.swing.JTextField txtApellidos;
    private javax.swing.JTextPane txtArea;
    private javax.swing.JTextField txtCargo;
    private javax.swing.JFormattedTextField txtCedula;
    private javax.swing.JTextField txtCiudad;
    private javax.swing.JTextField txtCorreo;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtFiltroBusqueda;
    private javax.swing.JTextField txtNombres;
    private javax.swing.JFormattedTextField txtTelefono;
    // End of variables declaration//GEN-END:variables
    private synchronized void mostrarRegistro() {
        System.out.println("archivos.frmEmpleado.mostrarRegistro()");
        if (jtEmpleados.getRowCount() == 0) {//Debe de haber un empleado seleccionado
            limpiarCampos();
            return;
        }
        //Mover Cursor...
        jtEmpleados.setRowSelectionInterval(cliEmpleados, cliEmpleados);

        txtCedula.setValue(new Categoria(//Colocando la cedula del empleado desde la tabla
                ((Categoria) jtEmpleados.getValueAt(cliEmpleados, 0)).getIdCategoria(),
                ((Categoria) jtEmpleados.getValueAt(cliEmpleados, 0)).getDescripcion())
        );

        txtNombres.setText(//Colocando el nombre del empleado desde la tabla
                (String) jtEmpleados.getValueAt(cliEmpleados, 1));
        txtApellidos.setText((String) jtEmpleados.getValueAt(cliEmpleados, 2));

        for (int i = 0; i < cbSexo.getItemCount(); i++) {//Colocando el sexo del empleado desde la tabla
            cbSexo.setSelectedIndex(i);
            if (((String) cbSexo.getSelectedItem()).equals(
                    ((String) jtEmpleados.getValueAt(cliEmpleados, 3)))) {
                break;
            }
        }

        txtTelefono.setValue(//Colocando el telefono del empleado desde la tabla
                jtEmpleados.getValueAt(cliEmpleados, 4));
        txtCorreo.setText(//Colocando el correo del empleado desde la tabla
                ((String) jtEmpleados.getValueAt(cliEmpleados, 5)).
                        equals("Sin Correo") ? ""
                : Utilidades.validaCorreo((String) jtEmpleados.getValueAt(cliEmpleados, 5))
                ? (String) jtEmpleados.getValueAt(cliEmpleados, 5) : "");
        txtCargo.setText(//Colocando el cargo del empleado desde la tabla
                ((String) jtEmpleados.getValueAt(cliEmpleados, 6)));

        try {//Colocando la fecha del empleado desde la tabla
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
            Date date = formatter.parse(jtEmpleados.getValueAt(cliEmpleados, 7).toString());
            dchFechaNacimiento.setDate(date);
        } catch (ParseException ex) {
            Logger.getLogger(frmEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        txtCiudad.setText(//Colocando la ciudad del empleado desde la tabla
                (String) jtEmpleados.getValueAt(cliEmpleados, 8));
        txtDireccion.setText(//Colocando la direccion del empleado desde la tabla
                (String) jtEmpleados.getValueAt(cliEmpleados, 9));
        cbEstado.setSelected(//Colocando el estado del empleado desde la tabla
                cbEstadoEmpleados.isSelected());

        verificarFotoHuella(txtCedula.getText().trim());
    }

    public synchronized void llenarTabla(boolean estado) {
        System.out.println("archivos.frmEmpleado.llenarTabla()");
        cliEmpleados = 0;
        jtEmpleados.removeAll();
        String titulos[] = {
            "<html><b>Cedula</b></html>",
            "<html><b>Nombres</b></html>",
            "<html><b>Apellidos</b></html>",
            "<html><b>Sexo</b></html>",
            "<html><b>Telefono</b></html>",
            "<html><b>Correo</b></html>",
            "<html><b>Cargo</b></html>",
            "<html><b>Fecha Nacimiento</b></html>",
            "<html><b>Ciudad</b></html>",
            "<html><b>Direccion</b></html>"};
        Object registro[] = new Object[titulos.length];
        try {
            ResultSet rs = getDatos("Llenar la tabla de los empleados").getEmpleadoEstado(estado);
            dtmEmpleado = new DefaultTableModel(null, titulos);
            while (rs.next()) {
                registro[0] = new Categoria(rs.getInt("idEmpleado"),
                        rs.getString("CEDULA").trim());
                registro[1] = rs.getString("NOMBRES").trim();
                registro[2] = rs.getString("APELLIDOS").trim();
                registro[3] = rs.getString("SEXO").trim().equals("m") ? "Masculino" : "Femenina";
                registro[4] = rs.getString("TELEFONO").trim();
                registro[5] = rs.getString("CORREO").trim();
                registro[6] = rs.getString("CARGO").trim();
                registro[7] = Utilidades.formatDate(rs.getDate("FECHANACIMIENTO"), "");
                registro[8] = rs.getString("Ciudad").trim();
                registro[9] = rs.getString("Direccion").trim();
                dtmEmpleado.addRow(registro);
            }
            jtEmpleados.setModel(dtmEmpleado);
            modeloOrdenado = new TableRowSorter<>(dtmEmpleado);
            jtEmpleados.setRowSorter(modeloOrdenado);
        } catch (SQLException ex) {
            LOGGER.getLogger(frmEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        adjustColumnPreferredWidths(jtEmpleados);
    }

    public void limpiarCampos() {
        if (!isShowing()) {
            return;
        }
        System.out.println("archivos.frmEmpleado.limpiarCampos()");
        txtNombres.setText("");
        txtApellidos.setText("");
        dchFechaNacimiento.setDate(null);
        cbSexo.setSelectedIndex(0);
        txtTelefono.setValue(null);
        txtCargo.setText("");
        txtCiudad.setText("");
        txtDireccion.setText("");
        txtCorreo.setText("");

        gifDinamico();
        txtArea.setText("");

        btnBorrarFoto.setEnabled(false);
        btnBorrarHuella.setEnabled(false);

        jlFoto.setText("Click para agregar foto");
        jlFoto.setIcon(null);

        txtCedula.setEditable(true);
        txtCedula.setValue(null);
        txtCedula.requestFocusInWindow();
        Utilidades.showTooltip(txtCedula);

    }

    private void navegador(boolean b) {
        System.out.println("archivos.frmEmpleado.navegador()");
        if (!nuevo) {
            txtCedula.setEditable(false);
            txtNombres.requestFocusInWindow();
        }

        btnPrimero.setEnabled(b);
        btnAnterior.setEnabled(b);
        btnSiguiente.setEnabled(b);
        btnUltimo.setEnabled(b);
        btnNuevo.setEnabled(b);
        btnModificar.setEnabled(b);
        btnBorrar.setEnabled(b);

        //Caja de Texto Habilitado
        btnCancelar.setEnabled(!b);
        btnGuardar.setEnabled(!b);
        btnValidaCedulaEmpleado.setEnabled(!b);
    }

    private synchronized void controlesEditable(boolean b) {
        System.out.println("archivos.frmEmpleado.controlesEditable()");
        txtNombres.setEditable(b);
        txtApellidos.setEditable(b);
        dchFechaNacimientoButton.setEnabled(b);//dchFechaNacimiento
        cbSexo.setEnabled(b);
        txtTelefono.setEditable(b);
        txtCargo.setEditable(b);
        editor.setEditable(b);
        txtCiudad.setEditable(b);
        txtDireccion.setEditable(b);
        cbEstado.setEnabled(b);
        txtCorreo.setEditable(b);

        btnValidaCedulaEmpleado.setEnabled(!b);
        txtCedula.setEditable(!b);
    }

    private void datosDetalles(boolean v) {
        System.out.println("archivos.frmEmpleado.datosDetalles()");
        jpDatos.setVisible(v);
        jpDetalles.setVisible(!v);
    }

    /**
     * *******************Metodos del lector de huellas***********************
     */
    private void iniciarLector() {
        System.out.println("archivos.frmEmpleado.iniciarLector()");
        gifDinamico();
        repaint();
        iniciar();
        start();
        estadoHuellas();
        stop();
    }

    private void start() {
        System.out.println("archivos.frmEmpleado.start()");
        Reclutador.clear();
        lector.startCapture();
        enviarTexto("Inicio del lector de huella", Color.green);
    }

    private void stop() {
        System.out.println("archivos.frmEmpleado.stop()");
        Reclutador.clear();
        lector.stopCapture();
        txtArea.setText("");
        enviarTexto("No se está usando el Lector de Huella Dactilar ", Color.red);
    }

    private void enviarTexto(String msg, Color c) {
        if (!lector.isStarted()) {
            return;
        }
        System.out.println("archivos.frmEmpleado.enviarTexto()");
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

        int len = txtArea.getDocument().getLength();
        txtArea.setCaretPosition(len);
        txtArea.setCharacterAttributes(aset, false);
        txtArea.replaceSelection(msg + "\n");
    }

    private void iniciar() {
        System.out.println("archivos.frmEmpleado.iniciar()");
        lector.addDataListener(new DPFPDataAdapter() {
            @Override
            public void dataAcquired(final DPFPDataEvent e) {
                SwingUtilities.invokeLater(() -> {
                    enviarTexto("La Huella Digital ha sido Capturada", Color.blue);
                    ProcesarCaptura(e.getSample());
                    huella = true;
                    borrarHuella = false;
                });
            }
        });

        lector.addReaderStatusListener(new DPFPReaderStatusAdapter() {
            @Override
            public void readerConnected(final DPFPReaderStatusEvent e) {
                SwingUtilities.invokeLater(() -> {
                    enviarTexto("El Sensor de Huella Digital esta Conectado", Color.green);
                });
            }

            @Override
            public void readerDisconnected(final DPFPReaderStatusEvent e) {
                SwingUtilities.invokeLater(() -> {
                    enviarTexto("El Sensor de Huella Digital esta NO CONECTADO", Color.red);
                });
            }
        });

        lector.addSensorListener(new DPFPSensorAdapter() {
            @Override
            public void fingerTouched(final DPFPSensorEvent e) {
                SwingUtilities.invokeLater(() -> {
                    enviarTexto("El dedo ha sido colocado sobre el Lector de Huella", Color.blue);
                });
            }

            @Override
            public void fingerGone(final DPFPSensorEvent e) {
                SwingUtilities.invokeLater(() -> {
                    enviarTexto("El dedo ha sido quitado del Lector de Huella", Color.orange);
                    identificarHuellaCancelar();
                });
            }
        });

        lector.addErrorListener(new DPFPErrorAdapter() {
            public void errorReader(final DPFPErrorEvent e) {
                SwingUtilities.invokeLater(() -> {
                    enviarTexto("Error: " + e.getError(), Color.red);
                });
            }
        });
    }

    private void DibujarHuella(Image image) {
        System.out.println("archivos.frmEmpleado.DibujarHuella()");
        jlImagenHuella.setIcon(new ImageIcon(
                image.getScaledInstance(jlImagenHuella.getWidth(),
                        jlImagenHuella.getHeight(), Image.SCALE_DEFAULT)));
        repaint();
    }

    private void ProcesarCaptura(DPFPSample sample) {
        System.out.println("archivos.frmEmpleado.ProcesarCaptura()");
        // Procesar la muestra de la huella y crear un conjunto de características con el propósito de inscripción.
        featuresinscripcion = extraerCaracteristicas(sample, DPFPDataPurpose.DATA_PURPOSE_ENROLLMENT);
        // Procesar la muestra de la huella y crear un conjunto de características con el propósito de verificacion.
        featuresverificacion = extraerCaracteristicas(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);
        // Comprobar la calidad de la muestra de la huella y lo añade a su reclutador si es bueno
        if (featuresinscripcion != null) {
            try {
                Reclutador.addFeatures(featuresinscripcion);// Agregar las caracteristicas de la huella a la plantilla a crear
                // Dibuja la huella dactilar capturada.
                DibujarHuella(CrearImagenHuella(sample));
                enviarTexto("Las Caracteristicas de la Huella han sido creada", Color.green);
            } catch (DPFPImageQualityException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), "Validación de proceso", JOptionPane.ERROR_MESSAGE);
                System.err.println("Error: " + ex.getMessage());
            } finally {
                // Comprueba si la plantilla se ha creado.
                switch (Reclutador.getTemplateStatus()) {
                    case TEMPLATE_STATUS_READY:	// informe de éxito y detiene  la captura de huellas
                        stop();
                        setPlantilla(Reclutador.getTemplate());
                        enviarTexto("La Plantilla de la Huella ha Sido Creada, "
                                + "ya puede guardar", Color.blue);
                        btnBorrarHuella.setEnabled(true);
                        break;

                    case TEMPLATE_STATUS_FAILED: // informe de fallas y reiniciar la captura de huellas
                        Reclutador.clear();
                        stop();
                        setPlantilla(null);
                        JOptionPane.showInternalMessageDialog(
                                this,
                                "La Plantilla de la Huella no pudo ser creada, "
                                + "Repita el Proceso",
                                "Inscripcion de Huellas Dactilares",
                                JOptionPane.ERROR_MESSAGE);
                        start();
                        break;
                }
                estadoHuellas();
            }
        }
    }

    private void estadoHuellas() {
        if (!lector.isStarted()) {
            return;
        }
        System.out.println("archivos.frmEmpleado.estadoHuellas()");
        enviarTexto("Muestra de Huellas Necesarias para Guardar Plantilla "
                + Reclutador.getFeaturesNeeded(), Color.blue);
    }

    private void identificarHuellaCancelar() {
        if (!lector.isStarted()) {
            return;
        }
        System.out.println("archivos.frmEmpleado.identificarHuellaCancelar()");
        try {
            //Obtiene todas las huellas de la bd
            PreparedStatement stmt = getDatos("Identificar huellas").getConn().
                    prepareStatement("SELECT (nombres||' '||apellidos) as nombres, "
                            + "huella "
                            + "FROM t_empleados "
                            + "WHERE huella is not null ");
            ResultSet rs = stmt.executeQuery();
            //Si se encuentra el nombre en la base de datos
            while (rs.next()) {
                //Lee la plantilla de la base de datos
                byte templateBuffer[] = rs.getBytes("huella");
                String nombre = rs.getString("nombres");
                //Crea una nueva plantilla a partir de la guardada en la base de datos
                DPFPTemplate referencePlantilla = DPFPGlobal.getTemplateFactory().createTemplate(templateBuffer);
                //Envia la plantilla creada al objeto contendor de Plantilla del componente de huella digital
                setPlantilla(referencePlantilla);

                // Compara las caracteriticas de la huella recientemente capturda con la
                // alguna plantilla guardada en la base de datos que coincide con ese tipo
                if (featuresverificacion == null || getPlantilla() == null) {
                    System.out.println("****Vinieron nulas featuresVerificacion y getPlantilla****");
                    return;
                }
                DPFPVerificationResult result = Verificador.verify(featuresverificacion, getPlantilla());

                //compara las plantilas (actual vs bd)
                //Si encuentra correspondencia dibuja el mapa
                //e indica el nombre de la persona que coincidió.
                if (result.isVerified()) {
                    //crea la imagen de los datos guardado de las huellas guardadas en la base de datos
                    btnCancelarActionPerformed(null);
                    JOptionPane.showInternalMessageDialog(
                            this,
                            "Las huella capturada es de " + nombre,
                            "Verificación de Huella",
                            JOptionPane.INFORMATION_MESSAGE);
                }
            }
        } catch (SQLException e) {
            //Si ocurre un error lo indica en la consola
            System.err.println("Error al identificar huella dactilar." + e.getMessage());
        }
    }

    private DPFPFeatureSet extraerCaracteristicas(DPFPSample sample,
            DPFPDataPurpose purpose) {
        System.out.println("archivos.frmEmpleado.extraerCaracteristicas()");
        DPFPFeatureExtraction extractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
        try {
            return extractor.createFeatureSet(sample, purpose);
        } catch (DPFPImageQualityException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    private Image CrearImagenHuella(DPFPSample sample) {
        System.out.println("archivos.frmEmpleado.CrearImagenHuella()");
        return DPFPGlobal.getSampleConversionFactory().createImage(sample);
    }

    private void gifDinamico() {
        System.out.println("archivos.frmEmpleado.gifDinamico()");
        Image image = new javax.swing.ImageIcon(getClass().getResource("/imagenes/huellas.gif")).getImage();
        jlImagenHuella.setIcon(new ImageIcon(
                image.getScaledInstance(jlImagenHuella.getWidth(),
                        jlImagenHuella.getHeight(), Image.SCALE_DEFAULT)));
    }

    private void verificarFotoHuella(String cedula) {
        if (fotoMap.containsKey(cedula)) {
            jlFoto.setText("");
            jlFoto.setIcon(new ImageIcon(fotoMap.get(cedula)
                    .getScaledInstance(180, 180, Image.SCALE_SMOOTH)));
            btnBorrarFoto.setEnabled(true);
        } else {

            //Aqui deberia de buscar la ultima foto del empleado en la 
            //Carpeta de foto antes de ir a la base de datos.
            ResultSet p = getDatos("Obteniendo path de imagen de la ruta ed una foto Local").getPathFoto(cedula);
            File f = null;
            try {
                while (p.next()) {
                    f = new File("foto/" + p.getString("NOMBRE_ARCHIVO"));
                }
            } catch (SQLException ex) {
                Logger.getLogger(frmEmpleado.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            try{
                jlFoto.setIcon(new ImageIcon(f.getAbsolutePath()));
            }catch(NullPointerException e){
                System.out.println(Colores.ANSI_YELLOW+Colores.ANSI_BLACK_BACKGROUND+"Error de la foto..."+Colores.ANSI_RESET);
            }
            

            if (jlFoto.getIcon() != null) {
                return;
            }

            Image img = getDatos("Obteniendo una imagen del usuario").
                    getImagenes("select FOTO "
                            + "from T_EMPLEADOS "
                            + "where Cedula = '" + cedula + "';");
            if (img != null) {
                fotoMap.put(cedula, img);
                jlFoto.setText("");
                jlFoto.setIcon(new ImageIcon(fotoMap.get(cedula)
                        .getScaledInstance(180, 180, Image.SCALE_SMOOTH)));
                btnBorrarFoto.setEnabled(true);
            } else {
                jlFoto.setText("Click para agregar foto");
                jlFoto.setIcon(null);
                btnBorrarFoto.setEnabled(false);
            }
        }

        if (getDatos("Investigando si el empleado tiene huella").tieneHuella(
                cedula)) {
            btnBorrarHuella.setEnabled(true);
            stop();
            estadoHuellas();
        } else {
            btnBorrarHuella.setEnabled(false);
            start();
            estadoHuellas();
        }

        gifDinamico();
    }
}
