package archivos;

import java.io.ByteArrayInputStream;
import java.io.File;



public class Empleado {
    private int idEmpleado;
    private String cedula;
    private String nombres;
    private String apellidos;
    private char sexo;
    private String telefono;
    private String cargo;
    private String fechaNacimiento;
    private String ciudad;
    private String direccion;
    private boolean estado;
    private ByteArrayInputStream huella;
    private int tamHuella;
    private File foto;
    private String correo;

    public Empleado(int idEmpleado, String cedula, String nombres, 
            String apellidos, char sexo, String telefono, String Cargo, 
            String fechaNacimiento, String ciudad, String direccion, 
            boolean estado, ByteArrayInputStream huella, int tamHuella,
            File foto, String correo) {
        this.idEmpleado = idEmpleado;
        this.cedula = cedula;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.sexo = sexo;
        this.telefono = telefono;
        this.cargo = Cargo;
        this.fechaNacimiento = fechaNacimiento;
        this.ciudad = ciudad;
        this.direccion = direccion;
        this.estado = estado;
        this.huella = huella;
        this.tamHuella = tamHuella;
        this.foto = foto;
        this.correo = correo;
    }
    
    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String Cargo) {
        this.cargo = Cargo;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }
    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
    public boolean isEstado() {
        return estado;
    }
    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public ByteArrayInputStream getHuella() {
        return huella;
    }

    public void setHuella(ByteArrayInputStream huella) {
        this.huella = huella;
    }

    public int getTamHuella() {
        return tamHuella;
    }

    public void setTamHuella(int tamHuella) {
        this.tamHuella = tamHuella;
    }

    public File getFoto() {
        return foto;
    }

    public void setFoto(File foto) {
        this.foto = foto;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    @Override
    public String toString() {
        return nombres+" "+apellidos;
    }
}
